{ This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/. }

unit Ecore;

{$PACKRECORDS C}

interface
  uses
    ctypes, Eina, Eo
    {$IF NOT DEFINED(WINDOWS)}
    ,BaseUnix
    {$ENDIF NOT DEFINED(WINDOWS)}
    ;
  type
    ppcchar = ^pcchar;
  
  {** Ecore_Common.h **}
  
  const
    ECORE_CALLBACK_CANCEL = EINA_FALSE;
    ECORE_CALLBACK_RENEW = EINA_TRUE;
    ECORE_CALLBACK_PASS_ON = EINA_TRUE;
    ECORE_CALLBACK_DONE = EINA_FALSE;
    ECORE_EVENT_NONE = 0;
    ECORE_EVENT_SIGNAL_USER = 1;
    ECORE_EVENT_SIGNAL_HUP = 2;
    ECORE_EVENT_SIGNAL_EXIT = 3;
    ECORE_EVENT_SIGNAL_POWER = 4;
    ECOER_EVENT_SIGNAL_REALTIME = 5;
    ECORE_EVENT_MEMORY_STATE = 6;
    ECORE_EVENT_POWER_STATE = 7;
    ECORE_EVENT_LOCALE_CHANGED = 8;
    ECORE_EVENT_HOSTNAME_CHANGED = 9;
    ECORE_EVENT_SYSTEM_TIMEDATE_CHANGED = 10;
    ECORE_EVENT_COUNT = 11;
    ECORE_EXE_PRIORITY_INHERIT = 9999;
  type
    TEcore_Version = record // FIXME: actually Ecore_Version
      major: cint;
      minor: cint;
      micro: cint;
      revision: cint;
    end;
    PEcore_Version = ^TEcore_Version;
    Ecore_Task_Cb = function(data: Pointer): Eina_Bool; cdecl;
    // TODO: Ecore_Select_Function
    Ecore_Cb = procedure(data: Pointer); cdecl;
    Ecore_Data_Cb = function(data: Pointer): Pointer; cdecl;
    PEcore_Win32_Handler = Pointer;
    PEcore_Event_Handler = Pointer;
    PEcore_Event_Filter = Pointer;
    PEcore_Event = Pointer;
    TEcore_Event_Signal_User = record // FIXME: actually Ecore_Event_Signal_User
      number: cint;
      ext_data: Pointer;
      {$IF NOT DEFINED(WINDOWS)} // __lv2ppu__ has no FPC equivalent (yet) and apparently EXOTIC_NO_SIGNAL is not defined anywhere in any case
      data: tsiginfo;
      {$ENDIF NOT DEFINED(WINDOWS)}
    end;
    TEcore_Event_Signal_Hup = record // FIXME: actually Ecore_Event_Signal_Hup
      ext_data: Pointer;
      {$IF NOT DEFINED(WINDOWS)} // __lv2ppu__ has no FPC equivalent (yet) and apparently EXOTIC_NO_SIGNAL is not defined anywhere in any case
      data: tsiginfo;
      {$ENDIF NOT DEFINED(WINDOWS)}
    end;
    TEcore_Event_Signal_Exit = record // FIXME: actually Ecore_Event_Signal_Exit
      interrupt: Eina_Bool; // FIXME: set to 1 by default
      quit: Eina_Bool; // FIXME: set to 1 by default
      terminate: Eina_Bool; // FIXME: set to 1 by default
      ext_data: Pointer;
      {$IF NOT DEFINED(WINDOWS)} // __lv2ppu__ has no FPC equivalent (yet) and apparently EXOTIC_NO_SIGNAL is not defined anywhere in any case
      data: tsiginfo;
      {$ENDIF NOT DEFINED(WINDOWS)}
    end;
    TEcore_Event_Signal_Power = record // FIXME: actually Ecore_Event_Signal_Power
      ext_data: Pointer;
      {$IF NOT DEFINED(WINDOWS)} // __lv2ppu__ has no FPC equivalent (yet) and apparently EXOTIC_NO_SIGNAL is not defined anywhere in any case
      data: tsiginfo;
      {$ENDIF NOT DEFINED(WINDOWS)}
    end;
    TEcore_Event_Signal_Realtime = record // FIXME: actually Ecore_Event_Signal_Realtime
      num: cint;
      {$IF NOT DEFINED(WINDOWS)} // __lv2ppu__ has no FPC equivalent (yet) and apparently EXOTIC_NO_SIGNAL is not defined anywhere in any case
      data: tsiginfo;
      {$ENDIF NOT DEFINED(WINDOWS)}
    end;
    Ecore_Filter_Cb = function(data: Pointer; loop_data: Pointer; &type: cint; event: Pointer): Eina_Bool; cdecl;
    Ecore_End_Cb = procedure(user_data: Pointer; func_data: Pointer); cdecl;
    Ecore_Event_Handler_Cb = function(data: Pointer; &type: cint; event: Pointer): Eina_Bool; cdecl;
    Ecore_Memory_State = (
      ECORE_MEMORY_STATE_NORMAL,
      ECORE_MEMORY_STATE_LOW
    );
    Ecore_Power_State = (
      ECORE_POWER_STATE_MAINS,
      ECORE_POWER_STATE_BATTERY,
      ECORE_POWER_STATE_LOW//,
      //ECORE_POWER_STATE_CRITICAL,
      //ECORE_POWER_STATE_EMERGENCY
    );
    Ecore_Exe_Flags = (
      ECORE_EXE_NONE := 0,
      ECORE_EXE_PIPE_READ := 1,
      ECORE_EXE_PIPE_WRITE := 2,
      ECORE_EXE_PIPE_ERROR := 4,
      ECORE_EXE_PIPE_READ_LINE_BUFFERED := 8,
      ECORE_EXE_PIPE_ERROR_LINE_BUFFEDER := 16,
      ECORE_EXE_PIPE_AUTO := 32,
      ECORE_EXE_RESPAWN := 64,
      ECORE_EXE_USE_SH := 128,
      ECORE_EXE_NOT_LEADER := 256,
      ECORE_EXE_TERM_WITH_PARENT := 512
    );
    Ecore_Exe_Win32_Priority = (
      ECORE_EXE_WIN32_PRIORITY_IDLE,
      ECORE_EXE_WIN32_PRIORITY_BELOW_NORMAL,
      ECORE_EXE_WIN32_PRIORITY_NORMAL,
      ECORE_EXE_WIN32_PRIORITY_ABOVE_NORMAL,
      ECORE_EXE_WIN32_PRIORITY_HIGH,
      ECORE_EXE_WIN32_PRIORITY_REALTIME
    );
    PEcore_Exe = PEo;
    Ecore_Exe_Cb = procedure(data: Pointer; exe: PEcore_Exe); cdecl;
    TEcore_Exe_Event_Add = record // FIXME: actually Ecore_Exe_Event_Add
      exe: PEcore_Exe;
      ext_data: Pointer;
    end;
    TEcore_Exe_Event_Del = record // FIXME: actually Ecore_Exe_Event_Del
      pid: pid_t;
      exit_code: cint;
      exe: PEcore_Exe;
      exit_signal: cint;
      exited: Eina_Bool; // FIXME: set to 1 by default
      signalled: Eina_Bool; // FIXME: set to 1 by default
      ext_data: Pointer;
      {$IF NOT DEFINED(WINDOWS)} // __lv2ppu__ has no FPC equivalent (yet) and apparently EXOTIC_NO_SIGNAL is not defined anywhere in any case
      data: tsiginfo;
      {$ENDIF NOT DEFINED(WINDOWS)}
    end;
    TEcore_Exe_Event_Data_Line = record // FIXME: actually Ecore_Exe_Event_Data_Line
      line: pcchar;
      size: cint;
    end;
    PEcore_Exe_Event_Data_Line = ^TECore_Exe_Event_Data_Line;
    TEcore_Exe_Event_Data = record // FIXME: actually Ecore_Exe_Event_Data
      exe: PEcore_Exe;
      data: Pointer;
      size: cint;
      lines: PEcore_Exe_Event_Data_Line;
    end;
    PEcore_Exe_Event_Data = ^TEcore_Exe_Event_Data;
    PEcore_Fd_Handler = Pointer;
    Ecore_Fd_Handler_Flags = (
      ECORE_FD_READ := 1,
      ECORE_FD_WRITE := 2,
      ECORE_FD_ERROR := 4
    );
    Ecore_Fd_Cb = function(data: Pointer; fd_handler: PEcore_Fd_Handler): Eina_Bool; cdecl;
    Ecore_Fd_Prep_Cb = procedure(data: Pointer; fd_handler: PEcore_Fd_Handler); cdecl;
    Ecore_Win32_Handle_Cb = function(data: Pointer; wh: PEcore_Win32_Handler): Eina_Bool; cdecl;
    PEcore_Thread = Pointer;
    Ecore_Thread_Cb = procedure(data: Pointer; thread: PEcore_Thread); cdecl;
    Ecore_Thread_Notify_Cb = procedure(data: Pointer; thread: PEcore_Thread; msg_data: Pointer); cdecl;
    PEcore_Pipe = Pointer;
    Ecore_Pipe_Cb = procedure(data: Pointer; buffer: Pointer; nbyte: cuint); cdecl;
    Ecore_Poller_Type = (
      ECORE_POLLER_CORE := 0
    );
    PEcore_Poller = PEo;
    Ecore_Pos_Map = (
      ECORE_POS_MAP_LINEAR,
      ECORE_POS_MAP_ACCELERATE,
      ECORE_POS_MAP_DECELERATE,
      ECORE_POS_MAP_SINUSOIDAL,
      ECORE_POS_MAP_ACCELERATE_FACTOR,
      ECORE_POS_MAP_DECELERATE_FACTOR,
      ECORE_POS_MAP_SINUSOIDAL_FACTOR,
      ECORE_POS_MAP_DIVISOR_INTERP,
      ECORE_POS_MAP_BOUNCE,
      ECORE_POS_MAP_SPRING,
      ECORE_POS_MAP_CUBIC_BEZIER
    );
    Ecore_Animator_Source = (
      ECORE_ANIMATOR_SOURCE_TIMER,
      ECORE_ANIMATOR_SOURCE_CUSTOM
    );
    Ecore_Timeline_Cb = function(data: Pointer; pos: cdouble): Eina_Bool; cdecl;
    PEcore_Animator = PEo;
    PEcore_Timer = PEo;
    PEcore_Idler = PEo;
    PEcore_Idle_Enterer = PEo;
    PEcore_Idle_Exiter = PEo;
    PEcore_Job = PEo;
  var
    ecore_version: PEcore_Version; cvar; external 'ecore';
    ECORE_EXE_EVENT_ADD: cint; cvar; external 'ecore';
    ECORE_EXE_EVENT_DEL: cint; cvar; external 'ecore';
    ECORE_EXE_EVENT_DATA: cint; cvar; external 'ecore';
    ECORE_EXE_EVENT_ERROR: cint; cvar; external 'ecore';
  function ecore_init(): cint; cdecl; external 'ecore';
  function ecore_shutdown(): cint; cdecl; external 'ecore';
  // TODO: ECORE_VERSION_MAJOR
  // TODO: ECORE_VERSION_MINOR
  procedure ecore_main_loop_iterate(); cdecl; external 'ecore';
  function ecore_main_loop_iterate_may_block(may_block: cint): cint; cdecl; external 'ecore';
  // TODO: ecore_main_loop_select_func_set
  // TODO: ecore_main_loop_select_func_get
  function ecore_main_loop_glib_integrate(): Eina_Bool; cdecl; external 'ecore';
  procedure ecore_main_loop_glib_always_integrate_disable(); cdecl; external 'ecore';
  procedure ecore_main_loop_begin(); cdecl; external 'ecore';
  procedure ecore_main_loop_quit(); cdecl; external 'ecore';
  function ecore_main_loop_animator_ticked_get(): Eina_Bool; cdecl; external 'ecore';
  function ecore_main_loop_nested_get(): cint; cdecl; external 'ecore';
  function ecore_fork_reset_callback_add(func: Ecore_Cb; data: Pointer): Eina_Bool; cdecl; external 'ecore';
  function ecore_fork_reset_callback_del(func: Ecore_Cb; data: Pointer): Eina_Bool; cdecl; external 'ecore';
  procedure ecore_fork_reset(); cdecl; external 'ecore';
  procedure ecore_main_loop_thread_safe_call_async(callback: Ecore_Cb; data: Pointer); cdecl; external 'ecore';
  function ecore_main_loop_thread_safe_call_sync(callback: Ecore_Data_Cb; data: Pointer): Pointer; cdecl; external 'ecore';
  procedure ecore_main_loop_thread_safe_call_wait(wait: cdouble); cdecl; external 'ecore';
  function ecore_thread_main_loop_begin(): cint; cdecl; external 'ecore';
  function ecore_thread_main_loop_end(): cint; cdecl; external 'ecore';
  function ecore_event_handler_add(&type: cint; func: Ecore_Event_Handler_Cb; data: Pointer): PEcore_Event_Handler; cdecl; external 'ecore';
  function ecore_event_handler_del(event_handler: PEcore_Event_Handler): Pointer; cdecl; external 'ecore';
  function ecore_event_add(&type: cint; ev: Pointer; func_free: Ecore_End_Cb; data: Pointer): PEcore_Event; cdecl; external 'ecore';
  function ecore_event_del(event: PEcore_Event): Pointer; cdecl; external 'ecore';
  function ecore_event_handler_data_get(eh: PEcore_Event_Handler): Pointer; cdecl; external 'ecore';
  function ecore_event_handler_data_set(eh: PEcore_Event_Handler; data: Pointer): Pointer; cdecl; external 'ecore';
  function ecore_event_type_new(): cint; cdecl; external 'ecore';
  function ecore_event_filter_add(func_start: Ecore_Data_Cb; func_filter: Ecore_Filter_Cb; func_end: Ecore_End_Cb; data: Pointer): PEcore_Event_Filter; cdecl; external 'ecore';
  function ecore_event_filter_del(ef: PEcore_Event_Filter): Pointer; cdecl; external 'ecore';
  function ecore_event_current_type_get(): cint; cdecl; external 'ecore';
  function ecore_event_current_event_get(): Pointer; cdecl; external 'ecore';
  function ecore_memory_state_get(): Ecore_Memory_State; cdecl; external 'ecore';
  procedure ecore_memory_state_set(state: Ecore_Memory_State); cdecl; external 'ecore';
  function ecore_power_state_get(): Ecore_Power_State; cdecl; external 'ecore';
  procedure ecore_power_state_set(state: Ecore_Power_State); cdecl; external 'ecore';
  // TODO: _ECORE_EXE_EO_CLASS_TYPE
  procedure ecore_exe_run_priority_set(pri: cint); cdecl; external 'ecore';
  function ecore_exe_run_priority_get(): cint; cdecl; external 'ecore';
  function ecore_exe_run(exe_cmd: pcchar; data: Pointer): PEcore_Exe; cdecl; external 'ecore';
  function ecore_exe_pipe_run(exe_cmd: pcchar; flags: Ecore_Exe_Flags; data: Pointer): PEcore_Exe; cdecl; external 'ecore';
  procedure ecore_exe_callback_pre_free_set(exe: PEcore_Exe; func: Ecore_Exe_Cb); cdecl; external 'ecore';
  function ecore_exe_send(exe: PEcore_Exe; data: Pointer; size: cint): Eina_Bool; cdecl; external 'ecore';
  procedure ecore_exe_close_stdin(exe: PEcore_Exe); cdecl; external 'ecore';
  procedure ecore_exe_auto_limits_set(exe: PEcore_Exe; start_bytes: cint; end_bytes: cint; start_lines: cint; end_lines: cint); cdecl; external 'ecore';
  function ecore_exe_event_data_get(exe: PEcore_Exe; flags: Ecore_Exe_Flags): PEcore_Exe_Event_Data; cdecl; external 'ecore';
  procedure ecore_exe_event_data_free(data: PEcore_Exe_Event_Data); cdecl; external 'ecore';
  function ecore_exe_free(exe: PEcore_Exe): Pointer; cdecl; external 'ecore';
  function ecore_exe_pid_get(exe: PEcore_Exe): pid_t; cdecl; external 'ecore';
  procedure ecore_exe_tag_set(exe: PEcore_Exe; tag: pcchar); cdecl; external 'ecore';
  function ecore_exe_tag_get(exe: PEcore_Exe): pcchar; cdecl; external 'ecore';
  function ecore_exe_cmd_get(exe: PEcore_Exe): pcchar; cdecl; external 'ecore';
  function ecore_exe_data_get(exe: PEcore_Exe): Pointer; cdecl; external 'ecore';
  function ecore_exe_data_set(exe: PEcore_Exe; data: Pointer): Pointer; cdecl; external 'ecore';
  function ecore_exe_flags_get(exe: PEcore_Exe): Ecore_Exe_Flags; cdecl; external 'ecore';
  procedure ecore_exe_pause(exe: PEcore_Exe); cdecl; external 'ecore';
  procedure ecore_exe_continue(exe: PEcore_Exe); cdecl; external 'ecore';
  procedure ecore_exe_interrupt(exe: PEcore_Exe); cdecl; external 'ecore';
  procedure ecore_exe_quit(exe: PEcore_Exe); cdecl; external 'ecore';
  procedure ecore_exe_terminate(exe: PEcore_Exe); cdecl; external 'ecore';
  procedure ecore_exe_kill(exe: PEcore_Exe); cdecl; external 'ecore';
  procedure ecore_exe_signal(exe: PEcore_Exe; num: cint); cdecl; external 'ecore';
  procedure ecore_exe_hup(exe: PEcore_Exe); cdecl; external 'ecore';
  function ecore_main_fd_handler_add(fd: cint; flags: Ecore_Fd_Handler_Flags; func: Ecore_Fd_Cb; data: Pointer; buf_func: Ecore_Fd_Cb; buf_data: Pointer): PEcore_Fd_Handler; cdecl; external 'ecore';
  function ecore_main_fd_handler_file_add(fd: cint; flags: Ecore_Fd_Handler_Flags; func: Ecore_Fd_Cb; data: Pointer; buf_func: Ecore_Fd_Cb; buf_data: Pointer): PEcore_Fd_Handler; cdecl; external 'ecore';
  procedure ecore_main_fd_handler_prepare_callback_set(fd_handler: PEcore_Fd_Handler; func: Ecore_Fd_Prep_Cb; data: Pointer); cdecl; external 'ecore';
  function ecore_main_fd_handler_del(fd_handler: PEcore_Fd_Handler): Pointer; cdecl; external 'ecore';
  function ecore_main_fd_handler_fd_get(fd_handler: PEcore_Fd_Handler): cint; cdecl; external 'ecore';
  function ecore_main_fd_handler_active_get(fd_handler: PEcore_Fd_Handler; flags: Ecore_Fd_Handler_Flags): Eina_Bool; cdecl; external 'ecore';
  procedure ecore_main_fd_handler_active_set(fd_handler: PEcore_Fd_Handler; flags: Ecore_Fd_Handler_Flags); cdecl; external 'ecore';
  function ecore_main_win32_handler_add(h: Pointer; func: Ecore_Win32_Handle_Cb; data: Pointer): PEcore_Win32_Handler; cdecl; external 'ecore';
  function ecore_main_win32_handler_del(win32_handler: PEcore_Win32_Handler): Pointer; cdecl; external 'ecore';
  function ecore_time_get(): cdouble; cdecl; external 'ecore';
  function ecore_time_unix_get(): cdouble; cdecl; external 'ecore';
  function ecore_loop_time_get(): cdouble; cdecl; external 'ecore';
  procedure ecore_loop_time_set(t: cdouble); cdecl; external 'ecore';
  function ecore_thread_run(func_blocking: ECore_Thread_Cb; func_end: Ecore_Thread_Cb; func_cancel: Ecore_Thread_Cb; data: Pointer): PEcore_Thread; cdecl; external 'ecore';
  function ecore_thread_feedback_run(func_heavy: Ecore_Thread_Cb; func_notify: Ecore_Thread_Notify_Cb; func_end: Ecore_Thread_Cb; func_cancel: Ecore_Thread_Cb; data: Pointer; try_no_queue: Eina_Bool): PEcore_Thread; cdecl; external 'ecore';
  function ecore_thread_cancel(thread: PEcore_Thread): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_wait(thread: PEcore_Thread; wait: cdouble): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_check(thread: PEcore_Thread): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_feedback(thread: PEcore_Thread; msg_data: Pointer): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_reschedule(thread: PEcore_Thread): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_active_get(): cint; cdecl; external 'ecore';
  function ecore_thread_pending_get(): cint; cdecl; external 'ecore';
  function ecore_thread_pending_feedback_get(): cint; cdecl; external 'ecore';
  function ecore_thread_pending_total_get(): cint; cdecl; external 'ecore';
  function ecore_thread_max_get(): cint; cdecl; external 'ecore';
  procedure ecore_thread_max_set(num: cint); cdecl; external 'ecore';
  procedure ecore_thread_max_reset(); cdecl; external 'ecore';
  function ecore_thread_available_get(): cint; cdecl; external 'ecore';
  function ecore_thread_local_data_add(thread: PEcore_Thread; key: pcchar; value: Pointer; cb: Eina_Free_Cb; direct: Eina_Bool): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_local_data_set(thread: PEcore_Thread; key: pcchar; value: Pointer; cb: Eina_Free_Cb): Pointer; cdecl; external 'ecore';
  function ecore_thread_local_data_find(thread: PEcore_Thread; key: pcchar): Pointer; cdecl; external 'ecore';
  function ecore_thread_local_data_del(thread: PEcore_Thread; key: pcchar): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_global_data_add(key: pcchar; value: Pointer; cb: Eina_Free_Cb; direct: Eina_Bool): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_global_data_set(key: pcchar; value: Pointer; cb: Eina_Free_Cb): Pointer; cdecl; external 'ecore';
  function ecore_thread_global_data_find(key: pcchar): Pointer; cdecl; external 'ecore';
  function ecore_thread_global_data_del(key: pcchar): Eina_Bool; cdecl; external 'ecore';
  function ecore_thread_global_data_wait(key: pcchar; seconds: cdouble): Pointer; cdecl; external 'ecore';
  function ecore_pipe_add(handler: Ecore_Pipe_Cb; data: Pointer): PEcore_Pipe; cdecl; external 'ecore';
  function ecore_pipe_full_add(handler: Ecore_Pipe_Cb; data: Pointer; fd_read: cint; fd_write: cint; read_survive_fork: Eina_Bool; write_survive_fork: Eina_Bool): PEcore_Pipe; cdecl; external 'ecore';
  function ecore_pipe_del(p: PEcore_Pipe): Pointer; cdecl; external 'ecore';
  function ecore_pipe_write(p: PEcore_Pipe; buffer: Pointer; nbytes: cuint): Eina_Bool; cdecl; external 'ecore';
  procedure ecore_pipe_write_close(p: PEcore_Pipe); cdecl; external 'ecore';
  procedure ecore_pipe_read_close(p: PEcore_Pipe); cdecl; external 'ecore';
  function ecore_pipe_read_fd(p: PEcore_Pipe): cint; cdecl; external 'ecore';
  function ecore_pipe_write_fd(p: PEcore_Pipe): cint; cdecl; external 'ecore';
  procedure ecore_pipe_thaw(p: PEcore_Pipe); cdecl; external 'ecore';
  procedure ecore_pipe_freeze(p: PEcore_Pipe); cdecl; external 'ecore';
  function ecore_pipe_wait(p: PEcore_Pipe; message_count: cint; wait: cdouble): cint; cdecl; external 'ecore';
  procedure ecore_app_args_set(argc: cint; argv: ppcchar); cdecl; external 'ecore';
  procedure ecore_app_args_get(var argc: cint; var argv: ppcchar); cdecl; external 'ecore';
  procedure ecore_app_restart(); cdecl; external 'ecore';
  procedure ecore_app_no_system_modules(); cdecl; external 'ecore';
  procedure ecore_throttle_adjust(amount: cdouble); cdecl; external 'ecore';
  function ecore_throttle_get(): cdouble; cdecl; external 'ecore';
  // TODO?: _ECORE_POLLER_EO_CLASS_TYPE
  procedure ecore_poller_poll_interval_set(&type: Ecore_Poller_Type; poll_time: cdouble); cdecl; external 'ecore';
  function ecore_poller_poll_interval_get(&type: Ecore_Poller_Type): cdouble; cdecl; external 'ecore';
  // TODO?: _ECORE_ANIMATOR_EO_CLASS_TYPE
  procedure ecore_animator_frametime_set(frametime: cdouble); cdecl; external 'ecore';
  function ecore_animator_frametime_get(): cdouble; cdecl; external 'ecore';
  function ecore_animator_pos_map(pos: cdouble; map: Ecore_Pos_Map; v1: cdouble; v2: cdouble): cdouble; cdecl; external 'ecore';
  function ecore_animator_pos_map_n(pos: cdouble; map: Ecore_Pos_Map; v_size: cint; v: pcdouble): cdouble; cdecl; external 'ecore';
  procedure ecore_animator_source_set(source: Ecore_Animator_Source); cdecl; external 'ecore';
  function ecore_animator_source_get(): Ecore_Animator_Source; cdecl; external 'ecore';
  procedure ecore_animator_custom_source_tick_begin_callback_set(func: Ecore_Cb; data: Pointer); cdecl; external 'ecore';
  procedure ecore_animator_custom_source_tick_end_callback_set(func: Ecore_Cb; data: Pointer); cdecl; external 'ecore';
  procedure ecore_animator_custom_tick(); cdecl; external 'ecore';
  // TODO?: _ECORE_TIMER_EO_CLASS_TYPE
  function ecore_timer_precision_get(): cdouble; cdecl; external 'ecore';
  procedure ecore_timer_precision_set(precision: cdouble); cdecl; external 'ecore';
  function ecore_timer_dump(): pcchar; cdecl; external 'ecore';
  // TODO?: _ECORE_IDLER_EO_CLASS_TYPE
  // TODO?: _ECORE_IDLE_ENTERER_EO_CLASS_TYPE
  // TODO?: _ECORE_IDLE_EXITER_EO_CLASS_TYPE
  // TODO?: _ECORE_JOB_EO_CLASS_TYPE
  
  {$IF NOT DEFINED(EFL_NOLEGACY_API_SUPPORT)}
  
  {$ENDIF NOT DEFINED(EFL_NOLEGACY_API_SUPPORT)}
  {$IF DEFINED(EFL_EO_API_SUPPORT)}
  
  {$ENDIF DEFINED(EFL_EO_API_SUPPORT)}
implementation
end.

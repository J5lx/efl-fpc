{ This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/. }

unit Ecore_File;

{$PACKRECORDS C}

interface
  uses
    ctypes, Eina;
  type
    ppcchar = ^pcchar;
  type
    PEcore_File_Monitor = Pointer;
    PEcore_File_Download_Job = Pointer;
    Ecore_File_Event = (
      ECORE_FILE_EVENT_NONE,
      ECORE_FILE_EVENT_CREATED_FILE,
      ECORE_FILE_EVENT_CREATED_DIRECTORY,
      ECORE_FILE_EVENT_DELETED_FILE,
      ECORE_FILE_EVENT_DELETED_DIRECTORY,
      ECORE_FILE_EVENT_DELETED_SELF,
      ECORE_FILE_EVENT_MODIFIED,
      ECORE_FILE_EVENT_CLOSED
    );
    Ecore_File_Monitor_Cb = procedure(data: Pointer; em: PEcore_File_Monitor; event: Ecore_File_Event; path: pcchar); cdecl;
    Ecore_File_Download_Completion_Cb = procedure(data: Pointer; &file: pcchar; status: cint); cdecl;
    Ecore_File_Progress_Return = (
      ECORE_FILE_PROGRESS_CONTINUE := 0,
      ECORE_FILE_PROGRESS_ABORT := 1
    );
    Ecore_File_Download_Progress_Cb = function(data: Pointer; &file: pcchar; dltotal: clong; dlnow: clong; ultotal: clong; ulnow: clong): cint; cdecl;
  function ecore_file_init(): cint; cdecl; external 'ecore_file';
  function ecore_file_shutdown(): cint; cdecl; external 'ecore_file';
  function ecore_file_mod_time(&file: pcchar): clonglong; cdecl; external 'ecore_file';
  function ecore_file_size(&file: pcchar): clonglong; cdecl; external 'ecore_file';
  function ecore_file_exists(&file: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_is_dir(&file: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_mkdir(dir: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_mkdirs(dirs: ppcchar): cint; cdecl; external 'ecore_file';
  function ecore_file_mksubdirs(base: pcchar; subdirs: ppcchar): cint; cdecl; external 'ecore_file';
  function ecore_file_rmdir(dir: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_recursive_rm(dir: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_mkpath(path: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_mkpaths(paths: ppcchar): cint; cdecl; external 'ecore_file';
  function ecore_file_cp(src: pcchar; dst: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_mv(src: pcchar; dst: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_symlink(src: pcchar; dest: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_realpath(&file: pcchar): pcchar; cdecl; external 'ecore_file';
  function ecore_file_unlink(&file: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_remove(&file: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_file_get(path: pcchar): pcchar; cdecl; external 'ecore_file';
  function ecore_file_dir_get(path: pcchar): pcchar; cdecl; external 'ecore_file';
  function ecore_file_can_read(&file: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_can_write(&file: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_can_exec(&file: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_readlink(link: pcchar): pcchar; cdecl; external 'ecore_file';
  function ecore_file_ls(dir: pcchar): PEina_List; cdecl; external 'ecore_file';
  function ecore_file_app_exe_get(app: pcchar): pcchar; cdecl; external 'ecore_file';
  function ecore_file_escape_name(filename: pcchar): pcchar; cdecl; external 'ecore_file';
  function ecore_file_strip_ext(&file: pcchar): pcchar; cdecl; external 'ecore_file';
  function ecore_file_dir_is_empty(dir: pcchar): cint; cdecl; external 'ecore_file';
  function ecore_file_monitor_add(path: pcchar; func: Ecore_File_Monitor_Cb; data: Pointer): PEcore_File_Monitor; cdecl; external 'ecore_file';
  procedure ecore_file_monitor_del(ecore_file_monitor: PEcore_File_Monitor); cdecl; external 'ecore_file';
  function ecore_file_monitor_path_get(ecore_file_monitor: PEcore_File_Monitor): pcchar; cdecl; external 'ecore_file';
  function ecore_file_path_dir_exists(in_dir: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_app_installed(exe: pcchar): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_app_list(): PEina_List; cdecl; external 'ecore_file';
  function ecore_file_download(url: pcchar; dst: pcchar; completion_cb: Ecore_File_Download_Completion_Cb; progress_cb: Ecore_File_Download_Progress_Cb; data: Pointer; var job_ret: PEcore_File_Download_Job; headers: PEina_Hash): Eina_Bool; cdecl; external 'ecore_file';
  function ecore_file_download_full(url: pcchar; dst: pcchar; completion_cb: Ecore_File_Download_Completion_Cb; progress_cb: Ecore_File_Download_Progress_Cb; data: Pointer; var job_ret: PEcore_File_Download_Job; headers: PEina_Hash): Eina_Bool; cdecl; external 'ecore_file';
  procedure ecore_file_download_abort_all(); cdecl; external 'ecore_file';
  procedure ecore_file_download_abort(job: PEcore_File_Download_Job); cdecl; external 'ecore_file';
  function ecore_file_download_protocol_available(protocol: pcchar): Eina_Bool; cdecl; external 'ecore_file';
implementation
end.

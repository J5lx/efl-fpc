{ This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/. }

unit Eo;

{$PACKRECORDS C}

interface
  uses
    ctypes, Eina;

  const
    EO_VERSION = 2;
    EO_CALL_CACHE_SIZE = 1;
    EO_CALLBACK_PRIORITY_BEFORE = -100;
    EO_CALLBACK_PRIORITY_DEFAULT = 0;
    EO_CALLBACK_PRIORITY_AFTER = 100;
    EO_CALLBACK_STOP = EINA_FALSE;
    EO_CALLBACK_CONTINUE = EINA_TRUE;
  type
    PEo = Pointer;
    Eo_Class = PEo;
    PEo_Class = ^Eo_Class;
    Eo_Op_Type = (
      EO_OP_TYPE_INVALID := -1,
      EO_OP_TYPE_REGULAR := 0,
      EO_OP_TYPE_CLASS,
      EO_OP_TYPE_REGULAR_OVERRIDE,
      EO_OP_TYPE_CLASS_OVERRIDE
    );
    PEo_Event_Description2 = Pointer;
    Eo_Event_Cb = function(data: Pointer; obj: PEo; desc: PEo_Event_Description2; event_info: Pointer): Eina_Bool; cdecl;
    Eo_Op = cuint;
    Eo_Class_Type = (
      EO_CLASS_TYPE_REGULAR := 0,
      EO_CLASS_TYPE_REGULAR_NO_INSTANT,
      EO_CLASS_TYPE_INTERFACE,
      EO_CLASS_TYPE_MIXIN
    );
    Eo_Op_Description = record
      api_func: Pointer;
      func: Pointer;
      op_type: Eo_Op_Type;
    end;
    PEo_Op_Description = ^Eo_Op_Description;
    // TODO: Eo_Class_Description = record
    //   version: cuint;
    //   name: pcchar;
    //   &type: Eo_Class_Type;
    //   ops: record
    //     descs: PEo_Op_Description;
    //     count: csize_t;
    //   end;
    //   events: PPEo_Event_Description;
    //   data_size: csize_t;
    //   class_constructor: procedure(klass: PEo_Class); cdecl;
    //   class_destructor: procedure(klass: PEo_Class); cdecl;
    // end;
    Eo_Op_Call_Data = record
      obj: PEo;
      func: Pointer;
      data: Pointer;
    end;
    PEo_Op_Call_Data = ^Eo_Op_Call_Data;
    Eo_Call_Cache_Index = record
      klass: Pointer;
    end;
    Eo_Call_Cache_Entry = record
      func: Pointer;
    end;
    Eo_Call_Cache_Off = record
      off: cint;
    end;
    Eo_Call_Cache = record
      {$IF EO_CALL_CACHE_SIZE > 0}
      index: array[0 .. EO_CALL_CACHE_SIZE - 1] of Eo_Call_Cache_Index;
      entry: array[0 .. EO_CALL_CACHE_SIZE - 1] of Eo_Call_Cache_Entry;
      off: array[0 .. EO_CALL_CACHE_SIZE - 1] of Eo_Call_Cache_Off;
      {$IF EO_CALL_CACHE_SIZE > 1}
      next_slot: cint;
      {$ENDIF EO_CALL_CACHE_SIZE > 1}
      {$ENDIF EO_CALL_CACHE_SIZE > 0}
      op: Eo_Op;
    end;
    PEo_Call_Cache = ^Eo_Call_Cache;
    eo_key_data_free_func = procedure(ptr: Pointer); cdecl;
  const
    EO_NOOP = Eo_Op(0);
  var
    _eo_class_creation_lock: Eina_Spinlock; cvar; external 'eo';
    // TODO: eo_base.eo.h
    EO_DBG_INFO_TYPE: PEina_Value_Type; cvar; external 'eo';
  // TODO: EO_DBG_INFO_LIST_APPEND
  // TODO: EO_DBG_INFO_APPEND
  // TODO: eo_dbg_info_free
  // TODO: EO_EVENT_DESCRIPTION
  // TODO: EO_EVENT_DESCRIPTION_HOT
  // TODO: EO_DEFINE_CLASS
  // TODO: eo_class_new
  function eo_isa(obj: PEo; klass: PEo_Class): Eina_Bool; cdecl; external 'eo';
  function eo_class_name_get(klass: PEo_Class): pcchar; cdecl; external 'eo';
  function eo_init(): Eina_Bool; cdecl; external 'eo';
  function eo_shutdown(): Eina_Bool; cdecl; external 'eo';
  // TODO: EO_CLASS_DESCRIPTION_NOOPS
  // TODO: EO_CLASS_DESCRIPTION_OPS
  // TODO: EO_FUNC_CALL
  // TODO: EO_FUNC_COMMON_OP_FUNC
  // TODO: EO_FUNC_COMMON_OP
  // TODO: EO_FUNC_BODY
  // TODO: EO_VOID_FUNC_BODY
  // TODO: EO_FUNC_BODYV
  // TODO: EO_VOID_FUNC_BODYV
  // TODO: _EO_OP_API_ENTRY
  // TODO: EO_OP_FUNC
  // TODO: EO_OP_CLASS_FUNC
  // TODO: EO_OP_FUNC_OVERRIDE
  // TODO: EO_OP_CLASS_FUNC_OVERRIDE
  function _eo_api_op_id_get(api_func: Pointer): Eo_Op; cdecl; external 'eo';
  function _eo_call_resolve(func_name: pcchar; call: PEo_Op_Call_Data; callcache: PEo_Call_Cache; &file: pcchar; line: cint): Eina_Bool; cdecl; external 'eo';
  function _eo_do_start(obj: PEo; cur_klass: PEo_Class; is_super: Eina_Bool; eo_stack: Pointer): Eina_Bool; cdecl; external 'eo';
  procedure _eo_do_end(eo_stack: Pointer); cdecl; external 'eo';
  function _eo_add_end(eo_stack: Pointer): PEo; cdecl; external 'eo';
  function _eo_stack_get(): Pointer; cdecl; external 'eo';
  // TODO: _eo_do_common
  // TODO: _eo_do_common_ret
  // TODO: eo_do
  // TODO: eo_do_super
  // TODO: eo_do_ret
  // TODO: eo_do_super_ret
  // TODO: eo_do_part
  function eo_class_get(obj: PEo): PEo_Class; cdecl; external 'eo';
  // TODO: _eo_add_common
  // TODO: eo_add
  // TODO: eo_add_ref
  function _eo_add_internal_start(&file: pcchar; line: cint; klass_id: PEo_Class; parent: PEo; ref: Eina_Bool): PEo; cdecl; external 'eo';
  function eo_data_get(obj: PEo; klass: PEo_Class): Pointer; cdecl; external 'eo'; deprecated;
  function eo_data_scope_get(obj: PEo; klass: PEo_Class): Pointer; cdecl; external 'eo';
  // TODO: eo_data_xref
  // TODO: eo_data_ref
  function eo_data_xref_internal(&file: pcchar; line: cint; obj: PEo; klass: PEo_Class; ref_obj: PEo): Pointer; cdecl; external 'eo';
  // TODO: eo_data_xunref
  // TODO: eo_data_unref
  procedure eo_data_xunref_internal(obj: PEo; data: Pointer; ref_obj: PEo); cdecl; external 'eo';
  function eo_ref(obj: PEo): PEo; cdecl; external 'eo';
  procedure eo_unref(obj: PEo); cdecl; external 'eo';
  function eo_ref_get(obj: PEo): cint; cdecl; external 'eo';
  procedure eo_del(obj: PEo); cdecl; external 'eo';
  // TODO: eo_xref
  function eo_xref_internal(&file: pcchar; line: cint; obj: PEo; ref_obj: PEo): PEo; cdecl; external 'eo';
  procedure eo_xunref(obj: PEo; ref_obj: PEo); cdecl; external 'eo';
  procedure eo_manual_free_set(obj: PEo; manual_free: Eina_Bool); cdecl; external 'eo';
  function eo_manual_free(obj: PEo): Eina_Bool; cdecl; external 'eo';
  function eo_destructed_is(obj: PEo): Eina_Bool; cdecl; external 'eo';
  // TODO: eo_abstract_class.eo.h
  // TODO: eo_weak_ref
  // TODO: eo_weak_unref
  // TODO: eo_wref_del_safe
  // TODO: eo_base_legacy_only_event_description_get
  // TODO: EO_CALLBACKS_ARRAY_DEFINE
  // TODO: eo_event_callback_add
  // TODO: eo_event_callback_array_add
  // TODO: EO_EV_CALLBACK_ADD
  // TODO: EO_EV_CALLBACK_DEL
  // TODO: EO_EV_DEL
implementation
end.

{ This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/. }

unit Eolian;

{$PACKRECORDS C}

interface
  uses
    ctypes, Eina;
  type
    PEolian_Class = Pointer;
    PEolian_Function = Pointer;
    PEolian_Type = Pointer;
    PEolian_Function_Parameter = Pointer;
    PEolian_Implement = Pointer;
    PEolian_Constructor = Pointer;
    PEolian_Event = Pointer;
    PEolian_Expression = Pointer;
    PEolian_Variable = Pointer;
    PEolian_Struct_Type_Field = Pointer;
    PEolian_Enum_Type_Field = Pointer;
    PEolian_Declaration = Pointer;
    PEolian_Documentation = Pointer;
    Eolian_Function_Type = (
      EOLIAN_UNRESOLVED,
      EOLIAN_PROPERTY,
      EOLIAN_PROP_SET,
      EOLIAN_PROP_GET,
      EOLIAN_METHOD
    );
    PEolian_Function_Type = ^Eolian_Function_Type;
    Eolian_Parameter_Dir = (
      EOLIAN_IN_PARAM,
      EOLIAN_OUT_PARAM,
      EOLIAN_INOUT_PARAM
    );
    Eolian_Class_Type = (
      EOLIAN_CLASS_UNKNOWN_TYPE,
      EOLIAN_CLASS_REGULAR,
      EOLIAN_CLASS_ABSTRACT,
      EOLIAN_CLASS_MIXIN,
      EOLIAN_CLASS_INTERFACE
    );
    Eolian_Object_Scope = (
      EOLIAN_SCOPE_PUBLIC,
      EOLIAN_SCOPE_PRIVATE,
      EOLIAN_SCOPE_PROTECTED
    );
    Eolian_Type_Type = (
      EOLIAN_TYPE_UNKNOWN_TYPE,
      EOLIAN_TYPE_VOID,
      EOLIAN_TYPE_REGULAR,
      EOLIAN_TYPE_COMPLEX,
      EOLIAN_TYPE_POINTER,
      EOLIAN_TYPE_STRUCT,
      EOLIAN_TYPE_STRUCT_OPAQUE,
      EOLIAN_TYPE_ENUM,
      EOLIAN_TYPE_ALIAS,
      EOLIAN_TYPE_CLASS
    );
    Eolian_Expression_Type = (
      EOLIAN_EXPR_UNKNOWN := 0,
      EOLIAN_EXPR_INT,
      EOLIAN_EXPR_UINT,
      EOLIAN_EXPR_LONG,
      EOLIAN_EXPR_ULONG,
      EOLIAN_EXPR_LLONG,
      EOLIAN_EXPR_ULLONG,
      EOLIAN_EXPR_FLOAT,
      EOLIAN_EXPR_DOUBLE,
      EOLIAN_EXPR_STRING,
      EOLIAN_EXPR_CHAR,
      EOLIAN_EXPR_NULL,
      EOLIAN_EXPR_BOOL,
      EOLIAN_EXPR_NAME,
      EOLIAN_EXPR_UNARY,
      EOLIAN_EXPR_BINARY
    );
    Eolian_Expression_Mask = (
      EOLIAN_MASK_SINT   := 1 shl 0,
      EOLIAN_MASK_UINT   := 1 shl 1,
      EOLIAN_MASK_INT    := Integer(EOLIAN_MASK_SINT) or Integer(EOLIAN_MASK_UINT),
      EOLIAN_MASK_FLOAT  := 1 shl 2,
      EOLIAN_MASK_BOOL   := 1 shl 3,
      EOLIAN_MASK_STRING := 1 shl 4,
      EOLIAN_MASK_CHAR   := 1 shl 5,
      EOLIAN_MASK_NULL   := 1 shl 6,
      EOLIAN_MASK_NUMBER := Integer(EOLIAN_MASK_INT) or Integer(EOLIAN_MASK_FLOAT),
      EOLIAN_MASK_ALL    := Integer(EOLIAN_MASK_NUMBER) or Integer(EOLIAN_MASK_BOOL)
                         or Integer(EOLIAN_MASK_STRING) or Integer(EOLIAN_MASK_CHAR)
                         or Integer(EOLIAN_MASK_NULL)
    );
    Eolian_Variable_Type = (
      EOLIAN_VAR_UNKNOWN := 0,
      EOLIAN_VAR_CONSTANT,
      EOLIAN_VAR_GLOBAL
    );
    Eolian_Value_Union = record
      case Integer of
        0: (c: cchar);
        1: (b: Eina_Bool);
        2: (s: pcchar);
        3: (i: csint);
        4: (u: cuint);
        5: (l: cslong);
        6: (ul: culong);
        7: (ll: cslonglong);
        8: (ull: culonglong);
        9: (f: cfloat);
        10: (d: cdouble);
    end;
    Eolian_Value = record
      &type: Eolian_Expression_Type;
      value: Eolian_Value_Union;
    end;
    PEolian_Value = ^Eolian_Value;
    Eolian_Binary_Operator = (
      EOLIAN_BINOP_INVALID := -1,
      
      EOLIAN_BINOP_ADD, { + int, float }
      EOLIAN_BINOP_SUB, { - int, float }
      EOLIAN_BINOP_MUL, { * int, float }
      EOLIAN_BINOP_DIV, { / int, float }
      EOLIAN_BINOP_MOD, { % int }
      
      EOLIAN_BINOP_EQ, { == all types }
      EOLIAN_BINOP_NQ, { != all types }
      EOLIAN_BINOP_GT, { >  int, float }
      EOLIAN_BINOP_LT, { <  int, float }
      EOLIAN_BINOP_GE, { >= int, float }
      EOLIAN_BINOP_LE, { <= int, float }
      
      EOLIAN_BINOP_AND, { && all types }
      EOLIAN_BINOP_OR,  { || all types }
      
      EOLIAN_BINOP_BAND, { &  int }
      EOLIAN_BINOP_BOR,  { |  int }
      EOLIAN_BINOP_BXOR, { ^  int }
      EOLIAN_BINOP_LSH,  { << int }
      EOLIAN_BINOP_RSH   { >> int }
    );
    Eolian_Unary_Operator = (
      EOLIAN_UNOP_INVALID := -1,
      
      EOLIAN_UNOP_UNM, { - sint }
      EOLIAN_UNOP_UNP, { + sint }
      
      EOLIAN_UNOP_NOT,  { ! int, float, bool }
      EOLIAN_UNOP_BNOT  { ~ int }
    );
    Eolian_Declaration_Type = (
      EOLIAN_DECL_UNKNOWN := -1,
      EOLIAN_DECL_CLASS,
      EOLIAN_DECL_ALIAS,
      EOLIAN_DECL_STRUCT,
      EOLIAN_DECL_ENUM,
      EOLIAN_DECL_VAR
    );

  function eolian_file_parse(filepath: pcchar): Eina_Bool; cdecl; external 'eolian';
  function eolian_all_eo_file_paths_get(): PEina_Iterator; cdecl; external 'eolian';
  function eolian_all_eot_file_paths_get(): PEina_Iterator; cdecl; external 'eolian';
  function eolian_all_eo_files_get(): PEina_Iterator; cdecl; external 'eolian';
  function eolian_all_eot_files_get(): PEina_Iterator; cdecl; external 'eolian';
  function eolian_init(): cint; cdecl; external 'eolian';
  function eolian_shutdown(): cint; cdecl; external 'eolian';
  function eolian_directory_scan(dir: pcchar): Eina_Bool; cdecl; external 'eolian';
  function eolian_system_directory_scan(): Eina_Bool; cdecl; external 'eolian';
  function eolian_all_eo_files_parse(): Eina_Bool; cdecl; external 'eolian';
  function eolian_all_eot_files_parse(): Eina_Bool; cdecl; external 'eolian';
  function eolian_database_validate(): Eina_Bool; cdecl; external 'eolian';
  function eolian_class_get_by_name(class_name: pcchar): PEolian_Class; cdecl; external 'eolian';
  function eolian_class_get_by_file(file_name: pcchar): PEolian_Class; cdecl; external 'eolian';
  function eolian_class_file_get(const klass: PEolian_Class): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_class_full_name_get(const klass: PEolian_Class): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_class_name_get(const klass: PEolian_Class): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_class_namespaces_get(const klass: PEolian_Class): PEina_Iterator; cdecl; external 'eolian';
  function eolian_class_type_get(const klass: PEolian_Class): Eolian_Class_Type; cdecl; external 'eolian';
  function eolian_all_classes_get(): PEina_Iterator; cdecl; external 'eolian';
  function eolian_class_documentation_get(const klass: PEolian_Class): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_class_legacy_prefix_get(const klass: PEolian_Class): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_class_eo_prefix_get(const klass: PEolian_Class): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_class_data_type_get(const klass: PEolian_Class): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_class_inherits_get(const klass: PEolian_Class): PEina_Iterator; cdecl; external 'eolian';
  function eolian_class_functions_get(const klass: PEolian_Class; func_type: Eolian_Function_Type): PEina_Iterator; cdecl; external 'eolian';
  function eolian_function_type_get(const function_id: PEolian_Function): Eolian_Function_Type; cdecl; external 'eolian';
  function eolian_function_scope_get(const function_id: PEolian_Function): Eolian_Object_Scope; cdecl; external 'eolian';
  function eolian_function_name_get(const function_id: PEolian_Function): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_function_full_c_name_get(const function_id: PEolian_Function; ftype: Eolian_Function_Type; use_legacy: Eina_Bool): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_class_function_get_by_name(const klass: PEolian_Class; func_name: pcchar; f_type: Eolian_Function_Type): PEolian_Function; cdecl; external 'eolian';
  function eolian_function_legacy_get(const function_id: PEolian_Function; f_type: Eolian_Function_Type): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_function_documentation_get(const function_id: PEolian_Function; f_type: Eolian_Function_Type): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_function_is_virtual_pure(const function_id: PEolian_Function; f_type: Eolian_Function_Type): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_is_auto(const function_id: PEolian_Function; f_type: Eolian_Function_Type): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_is_empty(const function_id: PEolian_Function; f_type: Eolian_Function_Type): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_is_legacy_only(const function_id: PEolian_Function; f_type: Eolian_Function_Type): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_is_class(const function_id: PEolian_Function): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_is_c_only(const function_id: PEolian_Function): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_is_beta(const function_id: PEolian_Function): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_is_constructor(const function_id: PEolian_Function; const klass: PEolian_Class): Eina_Bool; cdecl; external 'eolian';
  function eolias_function_parameters_get(const function_id: PEolian_Function): PEina_Iterator; cdecl; external 'eolian';
  function eolian_property_keys_get(const foo_id: PEolian_Function; ftype: Eolian_Function_Type): PEina_Iterator; cdecl; external 'eolian';
  function eolian_property_values_get(const foo_id: PEolian_Function; ftype: Eolian_Function_Type): PEina_Iterator; cdecl; external 'eolian';
  function eolian_parameter_direction_get(const param: PEolian_Function_Parameter): Eolian_Parameter_Dir; cdecl; external 'eolian';
  function eolian_parameter_type_get(const param: PEolian_Function_Parameter): PEolian_Type; cdecl; external 'eolian';
  function eolian_parameter_default_value_get(const param: PEolian_Function_Parameter): PEolian_Expression; cdecl; external 'eolian';
  function eolian_parameter_name_get(const param: PEolian_Function_Parameter): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_parameter_documentation_get(const param: PEolian_Function_Parameter): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_parameter_is_nonull(const param_desc: PEolian_Function_Parameter): Eina_Bool; cdecl; external 'eolian';
  function eolian_parameter_is_nullable(const param_desc: PEolian_Function_Parameter): Eina_Bool; cdecl; external 'eolian';
  function eolian_parameter_is_optional(const param_desc: PEolian_Function_Parameter): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_return_type_get(const function_id: PEolian_Function; ftype: Eolian_Function_Type): PEolian_Type; cdecl; external 'eolian';
  function eolian_function_return_default_value_get(const foo_id: PEolian_Function; ftype: Eolian_Function_Type): PEolian_Expression; cdecl; external 'eolian';
  function eolian_function_return_documentation_get(const foo_id: PEolian_Function; ftype: Eolian_Function_Type): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_function_return_is_warn_unused(const foo_id: PEolian_Function; ftype: Eolian_Function_Type): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_object_is_const(const function_id: PEolian_Function): Eina_Bool; cdecl; external 'eolian';
  function eolian_function_class_get(const function_id: PEolian_Function): PEolian_Class; cdecl; external 'eolian';
  function eolian_function_is_implemented(const function_id: PEolian_Function; func_type: Eolian_Function_Type; const klass: PEolian_Class): Eina_Bool; cdecl; external 'eolian';
  function eolian_implement_full_name_get(const impl: PEolian_Implement): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_implement_class_get(const impl: PEolian_Implement): PEolian_Class; cdecl; external 'eolian';
  function eolian_implement_function_get(const impl: PEolian_Implement; func_type: PEolian_Function_Type): PEolian_Function; cdecl; external 'eolian';
  function eolian_implement_is_auto(const impl: PEolian_Implement): Eina_Bool; cdecl; external 'eolian';
  function eolian_implement_is_empty(const impl: PEolian_Implement): Eina_Bool; cdecl; external 'eolian';
  function eolian_implement_is_virtual(const impl: PEolian_Implement): Eina_Bool; cdecl; external 'eolian';
  function eolian_implement_is_prop_get(const impl: PEolian_Implement): Eina_Bool; cdecl; external 'eolian';
  function eolian_implement_is_prop_set(const impl: PEolian_Implement): Eina_Bool; cdecl; external 'eolian';
  function eolian_class_implements_get(const klass: PEolian_Class): PEina_Iterator; cdecl; external 'eolian';
  function eolian_constructor_full_name_get(const ctor: PEolian_Constructor): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_constructor_class_get(const ctor: PEolian_Constructor): PEolian_Class; cdecl; external 'eolian';
  function eolian_constructor_function_get(const ctor: PEolian_Constructor): PEolian_Function; cdecl; external 'eolian';
  function eolian_constructor_is_optional(const ctor: PEolian_Constructor): Eina_Bool; cdecl; external 'eolian';
  function eolian_class_constructors_get(const klass: PEolian_Class): PEina_Iterator; cdecl; external 'eolian';
  function eolias_class_events_get(const klass: PEolian_Class): PEina_Iterator; cdecl; external 'eolian';
  function eolian_event_name_get(const event: PEolian_Event): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_event_type_get(const event: PEolian_Event): PEolian_Type; cdecl; external 'eolian';
  function eolian_event_documentation_get(const event: PEolian_Event): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_event_scope_get(const event: PEolian_Event): Eolian_Object_Scope; cdecl; external 'eolian';
  function eolian_event_is_beta(const event: PEolian_Event): Eina_Bool; cdecl; external 'eolian';
  function eolian_event_c_name_get(const event: PEolian_Event): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_class_event_get_by_name(const klass: PEolian_Class; event_name: pcchar): PEolian_Event; cdecl; external 'eolian';
  function eolian_class_ctor_enable_get(const klass: PEolian_Class): Eina_Bool; cdecl; external 'eolian';
  function eolian_class_dtor_enable_get(const klass: PEolian_Class): Eina_Bool; cdecl; external 'eolian';
  function eolian_class_c_get_function_name_get(const klass: PEolian_Class): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_alias_get_by_name(name: pcchar): PEolian_Type; cdecl; external 'eolian';
  function eolian_type_struct_get_by_name(name: pcchar): PEolian_Type; cdecl; external 'eolian';
  function eolian_type_enum_get_by_name(name: pcchar): PEolian_Type; cdecl; external 'eolian';
  function eolian_type_aliases_get_by_file(fname: pcchar): PEina_Iterator; cdecl; external 'eolian';
  function eolian_type_structs_get_by_file(fname: pcchar): PEina_Iterator; cdecl; external 'eolian';
  function eolian_type_enums_get_by_file(fname: pcchar): PEina_Iterator; cdecl; external 'eolian';
  function eolian_type_type_get(const tp: PEolian_Type): Eolian_Type_Type; cdecl; external 'eolian';
  function eolian_type_subtypes_get(const tp: PEolian_Type): PEina_Iterator; cdecl; external 'eolian';
  function eolian_type_struct_fields_get(const tp: PEolian_Type): PEina_Iterator; cdecl; external 'eolian';
  function eolian_type_struct_field_get(const tp: PEolian_Type): PEolian_Struct_Type_Field; cdecl; external 'eolian';
  function eolian_type_struct_field_name_get(const fl: PEolian_Struct_Type_Field): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_struct_field_documentation_get(const fl: PEolian_Struct_Type_Field): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_type_struct_field_type_get(const fl: PEolian_Struct_Type_Field): PEolian_Type; cdecl; external 'eolian';
  function eolian_type_enum_fields_get(const tp: PEolian_Type): PEina_Iterator; cdecl; external 'eolian';
  function eolian_type_enum_field_get(const tp: PEolian_Type; field: pcchar): PEolian_Enum_Type_Field; cdecl; external 'eolian';
  function eolian_type_enum_field_name_get(const fl: PEolian_Enum_Type_Field): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_enum_field_c_name_get(const fl: PEolian_Enum_Type_Field): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_enum_field_documentation_get(const fl: PEolian_Enum_Type_Field): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_type_enum_field_value_get(const fl: PEolian_Enum_Type_Field; force: Eina_Bool): PEolian_Expression; cdecl; external 'eolian';
  function eolian_type_enum_legacy_prefix_get(const tp: PEolian_Type): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_documentation_get(const tp: PEolian_Type): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_type_file_get(const tp: PEolian_Type): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_base_type_get(const tp: PEolian_Type): PEolian_Type; cdecl; external 'eolian';
  function eolian_type_class_get(const tp: PEolian_Type): PEolian_Class; cdecl; external 'eolian';
  function eolian_type_is_own(const tp: PEolian_Type): Eina_Bool; cdecl; external 'eolian';
  function eolian_type_is_const(const tp: PEolian_Type): Eina_Bool; cdecl; external 'eolian';
  function eolian_type_is_extern(const tp: PEolian_Type): Eina_Bool; cdecl; external 'eolian';
  function eolian_type_c_type_named_get(const tp: PEolian_Type; name: pcchar): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_c_type_get(const tp: PEolian_Type): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_name_get(const tp: PEolian_Type): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_full_name_get(const tp: PEolian_Type): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_type_namespaces_get(const tp: PEolian_Type): PEina_Iterator; cdecl; external 'eolian';
  function eolian_type_free_func_get(const tp: PEolian_Type): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_expression_eval(const expr: PEolian_Expression; m: Eolian_Expression_Mask): Eolian_Value; cdecl; external 'eolian';
  function eolian_expression_eval_type(const expr: PEolian_Expression; const &type: PEolian_Type): Eolian_Value; cdecl; external 'eolian';
  function eolian_expression_value_to_literal(const v: PEolian_Value): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_expression_serialize(const expr: PEolian_Expression): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_expression_type_get(const expr: PEolian_Expression): Eolian_Expression_Type; cdecl; external 'eolian';
  function eolian_expression_binary_operator_get(const expr: PEolian_Expression): Eolian_Binary_Operator; cdecl; external 'eolian';
  function eolian_expression_binary_lhs_get(const expr: PEolian_Expression): PEolian_Expression; cdecl; external 'eolian';
  function eolian_expression_binary_rhs_get(const expr: PEolian_Expression): PEolian_Expression; cdecl; external 'eolian';
  function eolian_expression_unary_operator_get(const expr: PEolian_Expression): Eolian_Unary_Operator; cdecl; external 'eolian';
  function eolian_expression_unary_expression_get(const expr: PEolian_Expression): PEolian_Expression; cdecl; external 'eolian';
  function eolian_expression_value_get(const expr: PEolian_Expression): Eolian_Value; cdecl; external 'eolian';
  function eolian_variable_global_get_by_name(name: pcchar): PEolian_Variable; cdecl; external 'eolian';
  function eolian_variable_constant_get_by_name(name: pcchar): PEolian_Variable; cdecl; external 'eolian';
  function eolian_variable_globals_get_by_file(fname: pcchar): PEina_Iterator; cdecl; external 'eolian';
  function eolian_variable_constants_get_by_file(fname: pcchar): PEina_Iterator; cdecl; external 'eolian';
  function eolian_variable_type_get(const &var: PEolian_Variable): Eolian_Variable_Type; cdecl; external 'eolian';
  function eolian_variable_documentation_get(const &var: PEolian_Variable): PEolian_Documentation; cdecl; external 'eolian';
  function eolian_variable_file_get(const &var: PEolian_Variable): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_variable_base_type_get(const &var: PEolian_Variable): PEolian_Type; cdecl; external 'eolian';
  function eolian_variable_value_get(const &var: PEolian_Variable): PEolian_Expression; cdecl; external 'eolian';
  function eolian_variable_name_get(const &var: PEolian_Variable): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_variable_full_name_get(const &var: PEolian_Variable): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_variable_namespaces_get(const &var: PEolian_Variable): PEina_Iterator; cdecl; external 'eolian';
  function eolian_variable_is_extern(const &var: PEolian_Variable): Eina_Bool; cdecl; external 'eolian';
  function eolian_declaration_get_by_name(name: pcchar): PEolian_Declaration; cdecl; external 'eolian';
  function eolian_declarations_get_by_file(fname: pcchar): PEina_Iterator; cdecl; external 'eolian';
  function eolian_declaration_type_get(const decl: PEolian_Declaration): Eolian_Declaration_Type; cdecl; external 'eolian';
  function eolian_declaration_name_get(const decl: PEolian_Declaration): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_declaration_class_get(const decl: PEolian_Declaration): PEolian_Class; cdecl; external 'eolian';
  function eolian_declaration_data_type_get(const decl: PEolian_Declaration): PEolian_Type; cdecl; external 'eolian';
  function eolian_declaration_variable_get(const decl: PEolian_Declaration): PEolian_Variable; cdecl; external 'eolian';
  function eolian_documentation_summary_get(const doc: PEolian_Documentation): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_documentation_description_get(const doc: PEolian_Documentation): PEina_Stringshare; cdecl; external 'eolian';
  function eolian_documentation_since_get(const doc: PEolian_Documentation): PEina_Stringshare; cdecl; external 'eolian';
implementation
end.

{ This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/. }

unit Eina;

{$PACKRECORDS C}

interface
  uses
    ctypes, pthreads;
  type
    // Technically va_list is compiler-dependent but this hack is used all over
    // the place in official FPC packages since apparently there's no other way
    va_list = Pointer;
  
  { eina_types.h }
  
  type
    Eina_Bool = cuchar;
    Eina_Compare_Cb = function(const data1: Pointer; const data2: Pointer): cint; cdecl;
    Eina_Random_Cb = function(const min: cint; const max: cint): cint; cdecl;
    Eina_Each_Cb = function(const container: Pointer; data: Pointer; fdata: Pointer): Eina_Bool; cdecl;
    Eina_Free_Cb = procedure(data: Pointer); cdecl;
  const
    EINA_FALSE = Eina_Bool(0);
    EINA_TRUE  = Eina_Bool(1);
  var
    eina_prime_table: array of cuint; cvar; external 'eina';
  // TODO?: EINA_C_ARRAY_LENGTH
  
  { eina_alloca.h }
  
  // Apparently specific to C, so no bindings
  
  { eina_main.h }
  
  // TODO: EINA_VERSION_MAJOR
  // TODO: EINA_VERSION_MINOR
  
  type
    TEina_Version = record
      // FIXME: should be Eina_Version, without leading T
      major: cint;
      minor: cint;
      micro: cint;
      revision: cint;
    end;
    PEina_Version = ^TEina_Version;

  var
    eina_version: PEina_Version; cvar; external 'eina';

  function eina_init(): cint; cdecl; external 'eina';
  function eina_shutdown(): cint; cdecl; external 'eina';
  function eina_threads_init(): cint; cdecl; external 'eina';
  function eina_threads_shutdown(): cint; cdecl; external 'eina';
  function eina_main_loop_is(): Eina_Bool; cdecl; external 'eina';
  procedure eina_main_loop_define(); cdecl; external 'eina';
  
  { eina_fp.h }
  
  const
    EINA_F32P32_PI = $00000003243f6A89;
    EINA_F16P16_ONE = 1 shl 16;
    EINA_F16P16_HALF = 1 shl 15;
  type
    Eina_F32p32 = cint64;
    Eina_F16p16 = cint32;
    Eina_F8p24 = cint32;
  // TODO: eina_f32p32_int_from
  // TODO: eina_f32p32_int_to
  // TODO: eina_f32p32_double_from
  // TODO: eina_f32p32_double_to
  // TODO: eina_f32p32_add
  // TODO: eina_f32p32_sub
  // TODO: eina_f32p32_mul
  // TODO: eina_f32p32_scale
  // TODO: eina_f32p32_div
  // TODO: eina_f32p32_sqrt
  // TODO: eina_f32p32_fracc_get
  // TODO: eina_fp32p32_llabs
  function eina_f32p32_cos(a: Eina_F32p32): Eina_F32p32; cdecl; external 'eina';
  function eina_f32p32_sin(a: Eina_F32p32): Eina_F32p32; cdecl; external 'eina';
  // TODO: eina_f16p16_int_from
  // TODO: eina_f16p16_int_to
  // TODO: eina_f16p16_double_from
  // TODO: eina_f16p16_double_to
  // TODO: eina_f16p16_float_from
  // TODO: eina_f16p16_float_to
  // TODO: eina_f16p16_add
  // TODO: eina_f16p16_sub
  // TODO: eina_f16p16_mul
  // TODO: eina_f16p16_scale
  // TODO: eina_f16p16_div
  // TODO: eina_f16p16_sqrt
  // TODO: eina_f16p16_fracc_get
  // TODO: eina_f8p24_int_from
  // TODO: eina_f8p24_int_to
  // TODO: eina_f8p24_float_from
  // TODO: eina_f8p24_float_to
  // TODO: eina_f8p24_add
  // TODO: eina_f8p24_sub
  // TODO: eina_f8p24_mul
  // TODO: eina_f8p24_scale
  // TODO: eina_f8p24_div
  // TODO: eina_f8p24_sqrt
  // TODO: eina_f8p24_fracc_get
  // TODO: eina_f16p16_to_f32p32
  // TODO: eina_f8p24_to_f32p32
  // TODO: eina_f32p32_to_f16p16
  // TODO: eina_f8p24_to_f16p16
  // TODO: eina_f32p32_to_f8p24
  // TODO: eina_f16p16_to_f8p24
  // TODO: eina_inline_f32p32.x
  // TODO: eina_inline_f16p16.x
  // TODO: eina_inline_f8p24.x
  // TODO: eina_inline_fp.x
  
  { eina_rectangle.h }
  
  const
    // TODO: EINA_RECTANGLE_INIT
    EINA_RECTANGLE_FORMAT = 'dx%d - %dx%d';
    // TODO: EINA_RECTANGLE_ARGS
  type
    Eina_Rectangle = record
      x: cint;
      y: cint;
      w: cint;
      h: cint;
    end;
    PEina_Rectangle = ^Eina_Rectangle;
    // TODO: Eina_Rectangle_Pool
    PEina_Rectangle_Pool = Pointer;
    Eina_Rectangle_Packing = (
      Eina_Packing_Descending,
      Eina_Packing_Ascending,
      Eina_Packing_Bottom_Left,
      Eina_Packing_Bottom_Left_Skyline,
      Eina_Packing_Bottom_Left_Skyline_Improved
    );
    // TODO: eina_spans_intersect
    // TODO: eina_rectangle_is_empty
    // TODO: eina_rectangle_coords_from
    // TODO: eina_rectangles_intersect
    // TODO: eina_rectangle_xcoord_inside
    // TODO: eina_rectangle_ycoord_inside
    // TODO: eina_rectangle_coords_inside
    // TODO: eina_rectangle_union
    // TODO: eina_rectangle_intersection
    // TODO: eina_rectangle_rescale_in
    // TODO: eina_rectangle_rescale_out
    // TODO: eina_rectangle_is_valid
    // TODO: eina_rectangle_max_x
    // TODO: eina_rectangle_max_y
    // TODO: eina_rectangle_x_cut
    // TODO: eina_rectangle_y_cut
    // TODO: eina_rectangle_width_cut
    // TODO: eina_rectangle_height_cut
    // TODO: eina_rectangle_subtract
    function eina_rectangle_pool_new(w: cint; h: cint): PEina_Rectangle_Pool; cdecl; external 'eina';
    function eina_rectangle_pool_get(rect: PEina_Rectangle): PEina_Rectangle_Pool; cdecl; external 'eina';
    function eina_rectangle_pool_geometry_get(pool: PEina_Rectangle_Pool; var w: cint; var h: cint): Eina_Bool; cdecl; external 'eina';
    function eina_rectangle_pool_data_get(pool: PEina_Rectangle_Pool): Pointer; cdecl; external 'eina';
    procedure eina_rectangle_pool_data_set(pool: PEina_Rectangle_Pool; data: Pointer); cdecl; external 'eina';
    procedure eina_rectangle_pool_free(pool: PEina_Rectangle_Pool); cdecl; external 'eina';
    function eina_rectangle_pool_count(pool: PEina_Rectangle_Pool): cint; cdecl; external 'eina';
    function eina_rectangle_pool_request(pool: PEina_Rectangle_Pool; w: cint; h: cint): PEina_Rectangle; cdecl; external 'eina';
    procedure eina_rectangle_pool_release(rect: PEina_Rectangle); cdecl; external 'eina';
    // TODO: EINA_RECTANGLE_SET
    function eina_rectangle_new(x: cint; y: cint; w: cint; h: cint): PEina_Rectangle; cdecl; external 'eina';
    procedure eina_rectangle_free(rect: PEina_Rectangle); cdecl; external 'eina';
    procedure eina_rectangle_pool_packing_set(pool: PEina_Rectangle_Pool; &type: Eina_Rectangle_Packing); cdecl; external 'eina';
    // TODO: eina_inline_rectangle.x
  
  { TODO: eina_clist.h }
  
  { TODO: eina_inlist.h }
  
  { TODO: eina_file.h }
  
  { eina_iterator.h }
  
  const
    EINA_ITERATOR_VERSION = 1;
    EINA_MAGIC_ITERATOR = $98761233;
  type
    // TODO: Eina_Iterator
    PEina_Iterator = Pointer;
    Eina_Iterator_Next_Callback = function(it: PEina_Iterator; var data: Pointer): Eina_Bool; cdecl;
    Eina_Iterator_Get_Container_Callback = function(it: PEina_Iterator): Pointer; cdecl;
    Eina_Iterator_Free_Callback = procedure(it: PEina_Iterator); cdecl;
    Eina_Iterator_Lock_Callback = function(it: PEina_Iterator): Eina_Bool; cdecl;
  procedure eina_iterator_free(iterator: PEina_Iterator); cdecl; external 'eina';
  function eina_iterator_container_get(iterator: PEina_Iterator): Pointer; cdecl; external 'eina';
  function eina_iterator_next(iterator: PEina_Iterator; var data: Pointer): Eina_Bool; cdecl; external 'eina';
  procedure eina_iterator_foreach(iterator: PEina_Iterator; callback: Eina_Each_Cb; fdata: Pointer); cdecl; external 'eina';
  function eina_iterator_lock(iterator: PEina_Iterator): Eina_Bool; cdecl; external 'eina';
  function eina_iterator_unlock(iterator: PEina_Iterator): Eina_Bool; cdecl; external 'eina';
  // TODO?: EINA_ITERATOR_FOREACH
  
  { eina_accessor.h }
  
  const
    EINA_ACCESSOR_VERSION = 2;
    EINA_MAGIC_ACCESSOR = $98761232;
  type
    // TODO: Eina_Accessor
    PEina_Accessor = Pointer;
    Eina_Accessor_Get_At_Callback = function(it: PEina_Accessor; idx: cuint; var data: Pointer): Eina_Bool; cdecl;
    Eina_Accessor_Get_Container_Callback = function(it: PEina_Accessor): Pointer; cdecl;
    Eina_Accessor_Free_Callback = procedure(it: PEina_Accessor); cdecl;
    Eina_Accessor_Lock_Callback = function(it: PEina_Accessor): Eina_Bool; cdecl;
    Eina_Accessor_Clone_Callback = function(it: PEina_Accessor): PEina_Accessor; cdecl;
  procedure eina_accessor_free(accessor: PEina_Accessor); cdecl; external 'eina';
  function eina_accessor_data_get(accessor: PEina_Accessor; position: cuint; var data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_accessor_container_get(accessor: PEina_Accessor): Pointer; cdecl; external 'eina';
  procedure eina_accessor_over(accessor: PEina_Accessor; cb: Eina_Each_Cb; start: cuint; &end: cuint; fdata: Pointer); cdecl; external 'eina';
  function eina_accessor_lock(accessor: PEina_Accessor): Eina_Bool; cdecl; external 'eina';
  function eina_accessor_clone(accessor: PEina_Accessor): PEina_Accessor; cdecl; external 'eina';
  function eina_accessor_unlock(accessor: PEina_Accessor): Eina_Bool; cdecl; external 'eina';
  // TODO: EINA_ACCESSOR_FOREACH
  
  { eina_list.h }
  
  type
    // TODO: Eina_List
    PEina_List = Pointer;
    // TODO: Eina_List_Accounting
    PEina_List_Accounting = Pointer;
  function eina_list_append(list: PEina_List; data: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_prepend(list: PEina_List; data: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_append_relative(list: PEina_List; data: Pointer; relative: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_append_relative_list(list: PEina_List; data: Pointer; relative: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_prepend_relative(list: PEina_List; data: Pointer; relative: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_prepend_relative_list(list: PEina_List; data: Pointer; relative: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_sorted_insert(list: PEina_List; func: Eina_Compare_Cb; data: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_remove(list: PEina_List; data: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_remove_list(list: PEina_List; remove_list: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_promote_list(list: PEina_List; move_list: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_demote_list(list: PEina_List; move_list: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_data_find(list: PEina_List; data: Pointer): Pointer; cdecl; external 'eina';
  function eina_list_data_find_list(list: PEina_List; data: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_move(var &to: PEina_List; var from: PEina_List; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_list_move_list(var &to: PEina_List; var from: PEina_List; data: PEina_List): Eina_Bool; cdecl; external 'eina';
  function eina_list_free(list: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_nth(list: PEina_List; n: cuint): Pointer; cdecl; external 'eina';
  function eina_list_nth_list(list: PEina_List; n: cuint): PEina_List; cdecl; external 'eina';
  function eina_list_reverse(list: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_reverse_clone(list: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_clone(list: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_sort(list: PEina_List; limit: cuint; func: Eina_Compare_Cb): PEina_List; cdecl; external 'eina';
  function eina_list_shuffle(list: PEina_List; func: Eina_Random_Cb): PEina_List; cdecl; external 'eina';
  function eina_list_merge(left: PEina_List; right: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_sorted_merge(left: PEina_List; right: PEina_List; func: Eina_Compare_Cb): PEina_List; cdecl; external 'eina';
  function eina_list_split_list(list: PEina_List; relative: PEina_List; var right: PEina_List): PEina_List; cdecl; external 'eina';
  function eina_list_search_sorted_near_list(list: PEina_List; func: Eina_Compare_Cb; data: Pointer; var result_cmp: cint): PEina_List; cdecl; external 'eina';
  function eina_list_search_sorted_list(list: PEina_List; func: Eina_Compare_Cb; data: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_search_sorted(list: PEina_List; func: Eina_Compare_Cb; data: Pointer): Pointer; cdecl; external 'eina';
  function eina_list_search_unsorted_list(list: PEina_List; func: Eina_Compare_Cb; data: Pointer): PEina_List; cdecl; external 'eina';
  function eina_list_search_unsorted(list: PEina_List; func: Eina_Compare_Cb; data: Pointer): Pointer; cdecl; external 'eina';
  // TODO: eina_list_last
  // TODO: eina_list_next
  // TODO: eina_list_prev
  // TODO: eina_list_data_get
  // TODO: eina_list_data_set
  // TODO: eina_list_count
  // TODO: eina_list_last_data_get
  function eina_list_iterator_new(list: PEina_List): PEina_Iterator; cdecl; external 'eina';
  function eina_list_iterator_reversed_new(list: PEina_List): PEina_Iterator; cdecl; external 'eina';
  function eina_list_accessor_new(list: PEina_List): PEina_Accessor; cdecl; external 'eina';
  function eina_list_data_idx(list: PEina_List; data: Pointer): cint; cdecl; external 'eina';
  // TODO: EINA_LIST_FOREACH
  // TODO: EINA_LIST_REVERSE_FOREACH
  // TODO: EINA_LIST_FOREACH_SAFE
  // TODO: EINA_LIST_REVERSE_FOREACH_SAFE
  // TODO: EINA_LIST_FREE
  // TODO: eina_inline_list.x
  
  { eina_hash.h }
  
  type
    PEina_Hash = Pointer;
    Eina_Hash_Tuple = record
      key: Pointer;
      data: Pointer;
      key_length: cuint;
    end;
    PEina_Hash_Tuple = ^Eina_Hash_Tuple;
    Eina_Key_Length = function(key: Pointer): cuint; cdecl;
    Eina_Key_Cmp = function(key1: Pointer; key1_length: cint; key2: Pointer; key2_length: cint): cint; cdecl;
    Eina_Key_Hash = function(key: Pointer; key_length: cint): cint; cdecl;
    Eina_Hash_Foreach = function(hash: PEina_Hash; key: Pointer; data: Pointer; fdata: Pointer): Eina_Bool; cdecl;
  function eina_hash_new(key_length_cb: Eina_Key_Length; key_cmp_cb: Eina_Key_Cmp; key_hash_cb: Eina_Key_Hash; data_free_cb: Eina_Free_Cb; buckets_power_size: cint): PEina_Hash; cdecl; external 'eina';
  procedure eina_hash_free_cb_set(hash: PEina_Hash; data_free_cb: Eina_Free_Cb); cdecl; external 'eina';
  function eina_hash_string_djb2_new(data_free_cb: Eina_Free_Cb): PEina_Hash; cdecl; external 'eina';
  function eina_hash_string_superfast_new(data_free_cb: Eina_Free_Cb): PEina_Hash; cdecl; external 'eina';
  function eina_hash_string_small_new(data_free_cb: Eina_Free_Cb): PEina_Hash; cdecl; external 'eina';
  function eina_hash_int32_new(data_free_cb: Eina_Free_Cb): PEina_Hash; cdecl; external 'eina';
  function eina_hash_int64_new(data_free_cb: Eina_Free_Cb): PEina_Hash; cdecl; external 'eina';
  function eina_hash_pointer_new(data_free_cb: Eina_Free_Cb): PEina_Hash; cdecl; external 'eina';
  function eina_hash_stringshared_new(data_free_cb: Eina_Free_Cb): PEina_Hash; cdecl; external 'eina';
  function eina_hash_add(hash: PEina_Hash; key: Pointer; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_hash_direct_add(hash: PEina_Hash; key: Pointer; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_hash_del(hash: PEina_Hash; key: Pointer; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_hash_find(hash: PEina_Hash; key: Pointer): Pointer; cdecl; external 'eina';
  function eina_hash_modify(hash: PEina_Hash; key: Pointer; data: Pointer): Pointer; cdecl; external 'eina';
  function eina_hash_set(hash: PEina_Hash; key: Pointer; data: Pointer): Pointer; cdecl; external 'eina';
  function eina_hash_move(hash: PEina_Hash; old_key: Pointer; new_key: Pointer): Eina_Bool; cdecl; external 'eina';
  procedure eina_hash_free(hash: PEina_Hash); cdecl; external 'eina';
  procedure eina_hash_free_buckets(hash: PEina_Hash); cdecl; external 'eina';
  function eina_hash_population(hash: PEina_Hash): cint; cdecl; external 'eina';
  function eina_hash_add_by_hash(hash: PEina_Hash; key: Pointer; key_length: cint; key_hash: cint; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_hash_direct_add_by_hash(hash: PEina_Hash; key: Pointer; key_length: cint; key_hash: cint; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_hash_del_by_key_hash(hash: PEina_Hash; key: Pointer; key_length: cint; key_hash: cint): Eina_Bool; cdecl; external 'eina';
  function eina_hash_del_by_key(hash: PEina_Hash; key: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_hash_del_by_data(hash: PEina_Hash; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_hash_del_by_hash(hash: PEina_Hash; key: Pointer; key_length: cint; key_hash: cint; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_hash_find_by_hash(hash: PEina_Hash; key: Pointer; key_length: cint; key_hash: cint): Pointer; cdecl; external 'eina';
  function eina_hash_modify_by_hash(hash: PEina_Hash; key: Pointer; key_length: cint; key_hash: cint; data: Pointer): Pointer; cdecl; external 'eina';
  function eina_hash_iterator_key_new(hash: PEina_Hash): PEina_Iterator; cdecl; external 'eina';
  function eina_hash_iterator_data_new(hash: PEina_Hash): PEina_Iterator; cdecl; external 'eina';
  function eina_hash_iterator_tuple_new(hash: PEina_Hash): PEina_Iterator; cdecl; external 'eina';
  // TODO: eina_hash_foreach
  procedure eina_hash_list_append(hash: PEina_Hash; key: Pointer; data: Pointer); cdecl; external 'eina';
  procedure eina_hash_list_prepend(hash: PEina_Hash; key: Pointer; data: Pointer); cdecl; external 'eina';
  procedure eina_hash_list_remove(hash: PEina_Hash; key: Pointer; data: Pointer); cdecl; external 'eina';
  function eina_hash_superfast(key: pcchar; len: cint): cint; cdecl; external 'eina';
  // TODO: eina_hash_djb2
  // TODO: eina_hasd_djb2_len
  // TODO: eina_hash_int32
  // TODO: eina_hash_int64
  // TODO: eina_hash_murmur3
  // TODO: eina_hash_crc
  // TODO: eina_inline_hash.x
  
  { TODO: eina_trash.h }
  
  { TODO: eina_lalloc.h }
  
  { TODO: eina_module.h }
  
  { TODO: eina_mempool.h }
  
  { eina_error.h }
  
  type
    Eina_Error = cint;
  var
    EINA_ERROR_OUT_OF_MEMORY: Eina_Error; cvar; external 'eina';
  function eina_error_msg_register(msg: pcchar): Eina_Error; cdecl; external 'eina';
  function eina_error_msg_static_register(msg: pcchar): Eina_Error; cdecl; external 'eina';
  function eina_error_msg_modify(error: Eina_Error; msg: pcchar): Eina_Bool; cdecl; external 'eina';
  function eina_error_get(): Eina_Error; cdecl; external 'eina';
  procedure eina_error_set(err: Eina_Error); cdecl; external 'eina';
  function eina_error_msg_get(error: Eina_Error): pcchar; cdecl; external 'eina';
  function eina_error_find(msg: pcchar): Eina_Error; cdecl; external 'eina';
  
  { TODO: eina_log.h }
  
  { eina_inarray.h }
  
  const
    EINA_ARRAY_VERSION = 1;
  type
    // TODO: Eina_Inarray
    PEina_Inarray = Pointer;
  function eina_inarray_new(member_size: cuint; step: cuint): PEina_Inarray; cdecl; external 'eina';
  procedure eina_inarray_free(&array: PEina_Inarray); cdecl; external 'eina';
  procedure eina_inarray_step_set(&array: PEina_Inarray; sizeof_eina_inarray: cuint; member_size: cuint; step: cuint); cdecl; external 'eina';
  procedure eina_inarray_flush(&array: PEina_Inarray); cdecl; external 'eina';
  function eina_inarray_push(&array: PEina_Inarray; data: Pointer): cint; cdecl; external 'eina';
  function eina_inarray_grow(&array: PEina_Inarray; size: cuint): Pointer; cdecl; external 'eina';
  function eina_inarray_insert(&array: PEina_Inarray; data: Pointer; compare: Eina_Compare_Cb): cint; cdecl; external 'eina';
  function eina_inarray_insert_sorted(&array: PEina_Inarray; data: Pointer; compare: Eina_Compare_Cb): cint; cdecl; external 'eina';
  function eina_inarray_remove(&array: PEina_Inarray; data: Pointer): cint; cdecl; external 'eina';
  function eina_inarray_pop(&array: PEina_Inarray): Pointer; cdecl; external 'eina';
  function eina_inarray_nth(&array: PEina_Inarray; position: cuint): Pointer; cdecl; external 'eina';
  function eina_inarray_insert_at(&array: PEina_Inarray; position: cuint; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_inarray_alloc_at(&array: PEina_Inarray; position: cuint; member_count: cuint): Pointer; cdecl; external 'eina';
  function eina_inarray_replace_at(&array: PEina_Inarray; position: cuint; data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_inarray_remove_at(&array: PEina_Inarray; position: cuint): Eina_Bool; cdecl; external 'eina';
  procedure eina_inarray_reverse(&array: PEina_Inarray); cdecl; external 'eina';
  procedure eina_inarray_sort(&array: PEina_Inarray; compare: Eina_Compare_Cb); cdecl; external 'eina';
  function eina_inarray_search(&array: PEina_Inarray; data: Pointer; compare: Eina_Compare_Cb): cint; cdecl; external 'eina';
  function eina_inarray_search_sorted(&array: PEina_Inarray; data: Pointer; compare: Eina_Compare_Cb): cint; cdecl; external 'eina';
  function eina_inarray_foreach(&array: PEina_Inarray; &function: Eina_Each_Cb; user_data: Pointer): Eina_Bool; cdecl; external 'eina';
  function eina_inarray_foreach_remove(&array: PEina_Inarray; match: Eina_Each_Cb; user_data: Pointer): cint; cdecl; external 'eina';
  function eina_inarray_resize(&array: PEina_Inarray; new_size: cuint): Eina_Bool; cdecl; external 'eina';
  function eina_inarray_count(&array: PEina_Inarray): cuint; cdecl; external 'eina';
  function eina_inarray_iterator_new(&array: PEina_Inarray): PEina_Iterator; cdecl; external 'eina';
  function eina_inarray_iterator_reversed_new(&array: PEina_Inarray): PEina_Iterator; cdecl; external 'eina';
  function eina_inarray_accessor_new(&array: PEina_Inarray): PEina_Accessor; cdecl; external 'eina';
  // TODO: EINA_INARRAY_FOREACH
  // TODO: EINA_INARRAY_REVERSE_FOREACH
  
  { TODO: eina_array.h }
  
  { TODO: eina_binshare.h }
  
  { eina_stringshare.h }
  
  type
    Eina_Stringshare = cchar;
    PEina_Stringshare = ^Eina_Stringshare;
    PPEina_Stringshare = ^PEina_Stringshare;

  function eina_stringshare_add_length(str: pcchar; slen: cuint): PEina_Stringshare; cdecl; external 'eina';
  function eina_stringshare_add(str: pcchar): PEina_Stringshare; cdecl; external 'eina';
  // TODO: eina_stringshare_printf
  // TODO: eina_stringshare_vprintf
  // TODO: eina_stringshare_nprintf
  function eina_stringshare_ref(str: PEina_Stringshare): PEina_Stringshare; cdecl; external 'eina';
  procedure eina_stringshare_del(str: PEina_Stringshare); cdecl; external 'eina';
  function eina_stringshare_strlen(str: PEina_Stringshare): cint; cdecl; external 'eina';
  procedure eina_stringshare_dump(); cdecl; external 'eina';
  // TODO: eina_stringshare_refplace
  // TODO: eina_stringshare_replace
  // TODO: eina_stringshare_replace_length
  
  { TODO: eina_ustringshare.h }
  
  { TODO: eina_magic.h }
  
  const
    EINA_MAGIC_NONE = $1234FEDC;
  type
    Eina_Magic = cuint;
  var
    EINA_ERROR_MAGIC_FAILED: Eina_Error; cvar; external 'eina';
  function eina_magic_string_get(magic: Eina_Magic): pcchar; cdecl; external 'eina';
  function eina_magic_string_set(magic: Eina_Magic; magic_name: pcchar): Eina_Bool; cdecl; external 'eina';
  function eina_magic_string_static_set(magic: Eina_Magic; magic_name: pcchar): Eina_Bool; cdecl; external 'eina';
  // TODO: EINA_MAGIC
  // TODO: EINA_MAGIC_SET
  // TODO: EINA_MAGIC_CHECK
  // TODO: EINA_MAGIC_FAIL
  // TODO: eina_magic_fail
  
  { TODO: eina_counter.h }
  
  { TODO: eina_rbtree.h }
  
  { TODO: eina_benchmark.h }
  
  { TODO: eina_convert.h }
  
  { TODO: eina_cpu.h }
  
  { TODO: eina_sched.h }
  
  { TODO: eina_tiler.h }
  
  { TODO: eina_thread.h }
  
  { TODO: eina_hamster.h }
  
  { TODO: eina_matrixsparse.h }
  
  { TODO: eina_str.h}
  
  { TODO: eina_strbuf.h }
  
  { TODO: eina_binbuf.h }
  
  { TODO: eina_ustrbuf.h }
  
  { TODO: eina_unicode.h }
  
  { TODO: eina_quadtree.h }
  
  { TODO: eina_simple_xml_parser.h }
  
  { eina_lock.h }
  
  type
    Eina_Lock_Result = (
      EINA_LOCK_FAIL := EINA_FALSE,
      EINA_LOCK_SUCCEED := EINA_TRUE,
      EINA_LOCK_DEADLOCK
    );
    Eina_TLS_Delete_Cb = procedure(ptr: Pointer); cdecl;
    Eina_Spinlock = pthread_spinlock_t; // TODO: Eina can use different spinlocks -> bindings should adjust
  // TODO: eina_inline_lock_posix.x
  var
    EINA_ERROR_NOT_MAIN_LOOP: Eina_Error; cvar; external 'eina';
  // TODO: eina_lock_new
  // TODO: eina_lock_free
  // TODO: eina_lock_take
  // TODO: eina_lock_take_try
  // TODO: eina_lock_release
  // TODO: eina_lock_debug
  // TODO: eina_condition_new
  // TODO: eina_condition_free
  // TODO: eina_condition_wait
  // TODO: eina_condition_timedwait
  // TODO: eina_condition_broadcast
  // TODO: eina_condition_signal
  // TODO: eina_rwlock_new
  // TODO: eina_rwlock_free
  // TODO: eina_rwlock_take_read
  // TODO: eina_rwlock_take_write
  // TODO: eina_rwlock_release
  // TODO: eina_tls_new
  // TODO: eina_tls_cb_new
  // TODO: eina_tls_free
  // TODO: eina_tls_get
  // TODO: eina_tls_set
  // TODO: eina_semaphore_new
  // TODO: eina_semaphore_free
  // TODO: eina_semaphore_lock
  // TODO: eina_semaphore_release
  // TODO: eina_barrier_new
  // TODO: eina_barrier_free
  // TODO: eina_barrier_wait
  // TODO: eina_spinlock_new
  // TODO: eina_spinlock_take
  // TODO: eina_spinlock_take_try
  // TODO: eina_spinlock_release
  // TODO: eina_spinlock_free
  // TODO: EINA_MAIN_LOOP_CHECK_RETURN_VAL
  // TODO: EINA_MAIN_LOOP_CHECK_RETURN
  
  { TODO: eina_prefix.h }
  
  { TODO: eina_refcount.h }
  
  { TODO: eina_mmap.h }
  
  { TODO: eina_xattr.h }
  
  { TODO: eina_value.h }
  
  const
    EINA_VALUE_BLOB_OPERATIONS_VERSION = 1;
    EINA_VALUE_STRUCT_OPERATIONS_VERSION = 1;
    EINA_VALUE_STRUCT_DESC_VERSION = 1;
    EINA_VALUE_TYPE_VERSION = 1;
  type
    PEina_Value = ^Eina_Value;
    PEina_Value_Type = ^Eina_Value_Type;
    Eina_Value_Union = record
      case Integer of
        0: (buf: array[0..7] of cuchar);
        1: (ptr: Pointer);
        2: (_guarantee: cuint64);
    end;
    Eina_Value = record
      &type: PEina_Value_Type;
      value: Eina_Value_Union;
    end;
    Eina_Value_Array = record
      subtype: PEina_Value_Type;
      step: cuint;
      &array: PEina_Inarray;
    end;
    Eina_Value_List = record
      subtype: PEina_Value_Type;
      list: PEina_List;
    end;
    Eina_Value_Hash = record
      subtype: PEina_Value_Type;
      buckets_power_size: cuint;
      hash: PEina_Hash;
    end;
    PEina_Value_Blob_Operations = ^Eina_Value_Blob_Operations;
    Eina_Value_Blob_Operations = record
      version: cuint;
      free: procedure(ops: PEina_Value_Blob_Operations; memory: Pointer; size: csize_t); cdecl;
      copy: function(ops: PEina_Value_Blob_Operations; memory: Pointer; size: csize_t): Pointer; cdecl;
      compare: function(ops: PEina_Value_Blob_Operations; data1: Pointer; size_data1: csize_t; data2: Pointer; size_data2: csize_t): cint; cdecl;
      to_string: function(ops: PEina_Value_Blob_Operations; memory: Pointer; size: csize_t): pcchar; cdecl;
    end;
    Eina_Value_Blob = record
      ops: PEina_Value_Blob_Operations;
      memory: Pointer;
      size: cuint;
    end;
    PEina_Value_Struct_Operations = ^Eina_Value_Struct_Operations;
    PEina_Value_Struct_Member = ^Eina_Value_Struct_Member;
    PEina_Value_Struct_Desc = ^Eina_Value_Struct_Desc;
    Eina_Value_Struct_Operations = record
      version: cuint;
      alloc: function(ops: PEina_Value_Struct_Operations; desc: PEina_Value_Struct_Desc): Pointer; cdecl;
      free: procedure(ops: PEina_Value_Struct_Operations; desc: PEina_Value_Struct_Desc; memory: Pointer); cdecl;
      copy: function(ops: PEina_Value_Struct_Operations; desc: PEina_Value_Struct_Desc; memory: Pointer): Pointer; cdecl;
      compare: function(ops: PEina_Value_Struct_Operations; desc: PEina_Value_Struct_Desc; data1: Pointer; data2: Pointer): cint; cdecl;
      find_member: function(ops: PEina_Value_Struct_Operations; desc: PEina_Value_Struct_Desc; name: pcchar): PEina_Value_Struct_Member;
    end;
    Eina_Value_Struct_Member = record
      name: pcchar;
      &type: PEina_Value_Type;
      offset: cuint;
    end;
    Eina_Value_Struct_Desc = record
      version: cuint;
      ops: PEina_Value_Struct_Operations;
      members: PEina_Value_Struct_Member;
      member_count: cuint;
      size: cuint;
    end;
    Eina_Value_Struct = record
      desc: PEina_Value_Struct_Desc;
      memory: Pointer;
    end;
    Eina_Value_Type = record
      version: cuint;
      value_size: cuint;
      name: pcchar;
      setup: function(&type: PEina_Value_Type; mem: Pointer): Eina_Bool; cdecl;
      flush: function(&type: PEina_Value_Type; mem: Pointer): Eina_Bool; cdecl;
      copy: function(&type: PEina_Value_Type; src: Pointer; dst: Pointer): Eina_Bool; cdecl;
      compare: function(&type: PEina_Value_Type; a: Pointer; b: Pointer): cint; cdecl;
      convert_to: function(&type: PEina_Value_Type; convert: PEina_Value_Type; type_mem: Pointer; convert_mem: Pointer): Eina_Bool; cdecl;
      convert_from: function(&type: PEina_Value_Type; convert: PEina_Value_Type; type_mem: Pointer; convert_mem: Pointer): Eina_Bool; cdecl;
      vset: function(&type: PEina_Value_Type; mem: Pointer; args: va_list): Eina_Bool; cdecl;
      pset: function(&type: PEina_Value_Type; mem: Pointer; ptr: Pointer): Eina_Bool; cdecl;
      pget: function(&type: PEina_Value_Type; mem: Pointer; ptr: Pointer): Eina_Bool; cdecl;
    end;
  var
    EINA_VALUE_TYPE_UCHAR: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_USHORT: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_UINT: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_ULONG: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_TIMESTAMP: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_UINT64: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_CHAR: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_SHORT: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_INT: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_LONG: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_INT64: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_FLOAT: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_DOUBLE: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_STRINGSHARE: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_STRING: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_ARRAY: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_LIST: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_HASH: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_TIMEVAL: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_BLOB: PEina_Value_Type; cvar; external 'eina';
    EINA_VALUE_TYPE_STRUCT: PEina_Value_Type; cvar; external 'eina';
    EINA_ERROR_VALUE_FAILED: Eina_Error; cvar; external 'eina';
    EINA_VALUE_BLOB_OPERATIONS_MALLOC: PEina_Value_Blob_Operations; cvar; external 'eina';
    EINA_VALUE_STRUCT_OPERATIONS_BINSEARCH: PEina_Value_Struct_Operations; cvar; external 'eina';
    EINA_VALUE_STRUCT_OPERATIONS_STRINGSHARE: PEina_Value_Struct_Operations; cvar; external 'eina';
  function eina_value_new(&type: PEina_Value_Type): PEina_Value; cdecl; external 'eina';
  procedure eina_value_free(value: PEina_Value); cdecl; external 'eina';
  // TODO: eina_value_setup
  // TODO: eina_value_flush
  function eina_value_copy(value: PEina_Value; copy: PEina_Value): Eina_Bool; cdecl; external 'eina';
  // TODO: eina_value_compare
  // TODO: eina_value_set
  // TODO: eina_value_get
  // TODO: eina_value_vset
  // TODO: eina_value_vget
  // TODO: eina_value_pset
  // TODO: eina_value_pget
  function eina_value_convert(value: PEina_Value; convert: PEina_Value): Eina_Bool; cdecl; external 'eina';
  function eina_value_to_string(value: PEina_Value): pcchar; cdecl; external 'eina';
  // TODO: eina_value_type_get
  function eina_value_array_new(subtype: PEina_Value_Type; step: cuint): PEina_Value; cdecl; external 'eina';
  // TODO: eina_value_array_setup
  // TODO: eina_value_array_count
  // TODO: eina_value_array_remove
  // TODO: eina_value_array_set
  // TODO: eina_value_array_get
  // TODO: eina_value_array_insert
  // TODO: eina_value_array_append
  // TODO: eina_value_array_vset
  // TODO: eina_value_array_vget
  // TODO: eina_value_array_vinsert
  // TODO: eina_value_array_vappend
  // TODO: eina_value_array_pset
  // TODO: eina_value_array_pget
  // TODO: eina_value_array_pinsert
  // TODO: eina_value_array_pappend
  // TODO: eina_value_array_value_get
  function eina_value_list_new(subype: PEina_Value_Type): PEina_Value; cdecl; external 'eina';
  // TODO: eina_value_list_setup
  // TODO: eina_value_list_count
  // TODO: eina_value_list_remove
  // TODO: eina_value_list_set
  // TODO: eina_value_list_get
  // TODO: eina_value_list_insert
  // TODO: eina_value_list_append
  // TODO: eina_value_list_vset
  // TODO: eina_value_list_vget
  // TODO: eina_value_list_vinsert
  // TODO: eina_value_list_vappend
  // TODO: eina_value_list_pset
  // TODO: eina_value_list_pget
  // TODO: eina_value_list_pinsert
  // TODO: eina_value_list_pappend
  function eina_value_hash_new(subtype: PEina_Value_Type; buckets_power_size: cuint): PEina_Value; cdecl; external 'eina';
  // TODO: eina_value_hash_setup
  // TODO: eina_value_hash_population
  // TODO: eina_value_hash_del
  // TODO: eina_value_hash_set
  // TODO: eina_value_hash_get
  // TODO: eina_value_hash_vset
  // TODO: eina_value_hash_vget
  // TODO: eina_value_hash_pset
  // TODO: eina_value_hash_pget
  // TODO: EINA_VALUE_STRUCT_MEMBER
  // TODO: EINA_VALUE_STRUCT_MEMBER_SENTINEL
  function eina_value_struct_new(desc: PEina_Value_Struct_Desc): PEina_Value; cdecl; external 'eina';
  // TODO: eina_value_struct_setup
  // TODO: eina_value_struct_set
  // TODO: eina_value_struct_get
  // TODO: eina_value_struct_vset
  // TODO: eina_value_struct_vget
  // TODO: eina_value_struct_pset
  // TODO: eina_value_struct_pget
  // TODO: eina_value_struct_value_get
  // TODO: eina_value_struct_value_set
  // TODO: eina_value_struct_member_value_get
  // TODO: eina_value_struct_member_value_set
  function eina_value_type_name_get(&type: PEina_Value_Type): pcchar; cdecl; external 'eina';
  function eina_value_type_check(&type: PEina_Value_Type): Eina_Bool; cdecl; external 'eina';
  // TODO: eina_value_type_setup
  // TODO: eina_value_type_flush
  // TODO: eina_value_type_copy
  // TODO: eina_value_type_compare
  // TODO: eina_value_type_convert_to
  // TODO: eina_value_type_convert_from
  // TODO: eina_value_type_vset
  // TODO: eina_value_type_pset
  // TODO: eina_value_type_pget
  // TODO: eina_inline_value.x
  
  { TODO: eina_value_util.h }
  
  { TODO: eina_cow.h }
  
  { TODO: eina_thread_queue.h }
  
  { TODO: eina_matrix.h }
  
  { TODO: eina_crc.h }
  
  { TODO: eina_evlog.h }
  
  { TODO: eina_util.h }
  
  { TODO: eina_quaternion.h }
  
  { TODO: eina_bezier.h }
implementation
end.

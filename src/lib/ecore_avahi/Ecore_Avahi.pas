{ This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/. }

unit Ecore_Avahi;

{$PACKRECORDS C}

interface
  uses
    ctypes;
  type
    PEcore_Avahi = Pointer;
  function ecore_avahi_add(): PEcore_Avahi; cdecl; external 'ecore_avahi';
  procedure ecore_avahi_del(handler: PEcore_Avahi); cdecl; external 'ecore_avahi';
  function ecore_avahi_poll_get(handler: PEcore_Avahi): Pointer; cdecl; external 'ecore_avahi';
implementation
end.

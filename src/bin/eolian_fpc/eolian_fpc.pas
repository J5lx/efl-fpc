{ This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/. }

program eolian_fpc;

{$IFDEF WINDOWS}
{$R eolian_fpc.rc}
{$ENDIF WINDOWS}

uses
  getopts, Eina, Eolian, SysUtils;
type
  TMode = (
    mNone,
    mHelp
  );
procedure ShowHelp();
begin
  writeln(Format('Usage: %s [-h/--help]', [argv[0]]));
  writeln('       --help/-h Print that help');
end;
var
  Opts: array[0..1] of TOption = (
    (Name: 'help';    Has_arg: No_Argument;       Flag: nil; Value: 'h'),
    (Name: #0;        Has_arg: 0;                 Flag: nil; Value: #0)
  );
  Opt: Char;
  Index: LongInt;
  Mode: TMode = mNone;
  ExitCode: LongInt = 0;
begin
  eina_init();
  eolian_init();

  while true do begin
    Opt := GetLongOpts('h', Opts, Index);
    case Opt of
      'h':
        Mode := mHelp;
      EndOfOptions:
        break;
    end;
  end;

  case Mode of
    mHelp:
      ShowHelp();
    mNone: begin
      writeln(StdErr, 'No mode of operation chosen');
      ExitCode := 1;
    end;
  end;

  eolian_shutdown();
  eina_shutdown();
  halt(ExitCode);
end.

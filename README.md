# efl-fpc

WIP EFL bindings for FreePascal. At the moment I'm still at a very early stage,
there are lots of TODOs and FIXMEs and parts that I haven't even started
working on. Also, I basically choose the parts I work on on a whim, and then I
just start writing code, so the code quality / consistency could sometimes be
better. For the time being, this shouldn't be used for any "serious" projects,
but taking a quick look won't hurt.

## Status

 Component          | Status
--------------------|-------------
 Ecore              | In Progress
 Ecore_Audio        | Not Started
 Ecore_Avahi        | Done
 Ecore_Con          | Not Started
 Ecore_Drm          | Not Started
 Ecore_Evas         | Not Started
 Ecore_Fb           | Not Started
 Ecore_File         | Done
 Ecore_IMF          | Not Started
 Ecore_IMF_Evas     | Not Started
 Ecore_Input        | Not Started
 Ecore_Input_Evas   | Not Started
 Ecore_Ipc          | Not Started
 Ecore_Wayland      | Not Started
 Ecore_X            | Not Started
 Ector              | Not Started
 Edje               | Not Started
 Eet                | Not Started
 Eeze               | Not Started
 Efl (common stuff) | Not Started
 Efreet             | Not Started
 Eina               | In Progress
 Eio                | Not Started
 Eldbus             | Not Started
 Elocation          | Not Started
 Elua               | Not Started
 Embryo             | Not Started
 Emile              | Not Started
 Emotion            | Not Started
 Eo                 | In Progress
 Eolian             | Done
 EPhysics           | Not Started
 Ethumb             | Not Started
 Ethumb_Client      | Not Started
 Evas               | Not Started

Some other things that aren't exactly parts of EFL but closely related to it or
otherwise relevant:

 Component                          | Status
------------------------------------|-------------
 Elementary                         | Not Started
 eolian_fpc (Eo bindings generator) | In Progress

## Building

```
fpcmake
make
```
